<div class="reveal">

<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">

<section>
    <h1>PHP</h1>
    <p>
    <ul>
        <li class="fragment">PHP - hypertext preprocessor</li>
        <li class="fragment">
            PHP (англ. PHP: Hypertext Preprocessor — «PHP: препроцессор гипертекста»;
            первоначально — «Инструменты для создания персональных веб-страниц»;
            произносится пи-эйч-пи) — скриптовый язык программирования общего назначения,
            интенсивно применяемый для разработки веб-приложений.
            В настоящее время поддерживается подавляющим большинством хостинг-провайдеров и является одним из лидеров среди языков программирования,
            применяющихся для создания динамических веб-сайтов.
        </li>
    </ul>
    </p>
</section>

    <section>
        <h3 class="fragment highlight-green">PHP вперемешку с HTML</h3>
        <pre>
            <?php
            highlight_string('
        <html>
            <head>
                <title>Пример</title>
            </head>
            <body>
                 <?php
                 echo "Привет, я - скрипт PHP!";
                 ?>
            </body>
        </html>');
            ?>
        </pre>
    </section>

    <section>
        <h3 class="fragment highlight-red">Ошибка?</h3>
        <pre>
            <?php
            highlight_string('
        <html>
            <head>
                <title>Пример</title>
            </head>
            <body>

                    echo "Привет, я - скрипт PHP!";
                 ?>
            </body>
        </html>');
            ?>
        </pre>
        <pre class="fragment">
            <?php
                highlight_string('
                <?php // - открывающий тег

                ?> // - закрывающий тег
                ');
            ?>
        </pre>
    </section>

    <section>
        <h3 class="fragment highlight-green">Разделение инструкций</h3>
        <pre>
            <?php
            highlight_string('
        <?php
            echo "Это тест";
        ?>

        <?php echo "Это тест" ?>
        ');
            ?>
        </pre>
    </section>


    <section>
        <h3 class="fragment highlight-red">Ошибка?</h3>
        <pre>
            <?php
            highlight_string('
        <?php
            echo "Это тест";
        ?>

            echo "Это тест" ?>
        ');
            ?>
        </pre>
        <pre class="fragment">
            <?php
            highlight_string('
                <?php // - открывающий тег

                ?> // - закрывающий тег
                ');
            ?>
        </pre>
    </section>

    <section>
        <h3>Комментарии в PHP скриптах</h3>
        <pre>
           <?php
           highlight_string('
   <?php
   echo "Это тест"; // Это однострочный комментарий в стиле c++
   /* Это многострочный комментарий
      еще одна строка комментария */
   echo "Это еще один тест";
   echo "Последний тест"; # Это комментарий в стиле оболочки Unix
   ?>
           ');
            ?>
        </pre>
    </section>


    <section>
        <h3>Переменные в PHP</h3>
        <p>переменная - это область оперативной памяти, доступ к которой осуществляется по имени.</p>
        <pre>
           <?php
           highlight_string('
           <?php
            $message = "Привет, я - скрипт PHP!";
            echo $message;
            ?>
           ');
           ?>
        </pre>
        <p>Имена всех переменных в PHP должны начинаться со знака $ — так интерпретатору значительно легче "понять" и отличить их</p>
    </section>

    <section>
        <h4>Типы данных в PHP</h4>
        <p class="fragment">PHP поддерживает восемь простых типов данных:</p>
        <ul class="fragment">Четыре скалярных типа:
            <li class="fragment"> - boolean (двоичные данные)
            <li class="fragment"> - integer (целые числа)
            <li class="fragment"> - float (числа с плавающей точкой или 'double')
            <li class="fragment"> - string (строки)
        </ul>
        </section>


    <section>
        <h4>Типы данных в PHP</h4>
        <p>Два смешанных типа:</p>
        <ul>
        <li class="fragment"> - array (массивы)
        <li class="fragment"> - object (объекты)
        </ul>

        <p class="fragment">И два специальных типа:</p>
        <ul>
            <li class="fragment"> resource (ресурсы)
            <li class="fragment"> NULL ("пустые")
        </ul>
    </section>


    <section>
        <h4>Типы данных в PHP</h4>
        <p>Существуют также несколько псевдотипов:</p>

        <ul>
            <li class="fragment"> - mixed (смешанные)
            <li class="fragment"> - number (числа)
            <li class="fragment"> - callback (обратного вызова)
            </ul>
    </section>

    <section>
        <h3>Двоичные данные (Boolean)</h3>
        <p>Это простейший тип. Он выражает истинность значения - это может быть либо TRUE, либо FALSE</p>
        <pre>
           <?php
           highlight_string('
         <?php
            var_dump((bool) "");        // bool(false)
            var_dump((bool) 1);         // bool(true)
            var_dump((bool) -2);        // bool(true)
            var_dump((bool) "foo");     // bool(true)
            var_dump((bool) 2.3e5);     // bool(true)
            var_dump((bool) array(12)); // bool(true)
            var_dump((bool) array());   // bool(false)
            var_dump((bool) "false");   // bool(true)
            ?>
           ');
           ?>
        </pre>
    </section>


<section>
    <h3>Двоичные данные (Boolean)</h3>
    <p class="fragment highlight-green">TRUE ?</p>
        <pre>
           <?php
           highlight_string('
         <?php
         $my_name = "Alexei Smolyanov";
         if($my_name)
            echo "TRUE";
         else
            echo "FALSE";
            ?>
           ');
           ?>
        </pre>
</section>


<section>
    <h3>Целые числа (Integer)</h3>
    <span>Целое - это число из множества Z = {..., -2, -1, 0, 1, 2, ...}, обычно длиной 32 бита (от –2 147 483 648 до 2 147 483 647).
        Целые могут быть указаны в десятичной, шестнадцатеричной или восьмеричной системе счисления, по желанию, с предшествующим знаком (- или +).</span>
        <pre>
           <?php
           highlight_string('
<?php
$a = 1234; // десятичное число
$a = -123; // отрицательное число
$a = 0123; // восьмеричное число (эквивалентно 83 в десятичной системе)
$a = 0x1A; // шестнадцатеричное число (эквивалентно 26 в десятичной системе)
    ?>
           ');
           ?>
        </pre>
</section>

<section>
    <h3>Числа с плавающей точкой (Float)</h3>
    <span>Double - вещественное число довольно большой точности (ее должно хватить для подавляющего большинства математических вычислений).

Числа с плавающей точкой (они же числа двойной точности или действительные числа) могут быть определены при помощи любого из следующих синтаксисов:</span>
        <pre>
           <?php
           highlight_string('
<?php
$a = 1.234;
$b = 1.2e3;
$c = 7E-10;
?>
           ');
           ?>
        </pre>
</section>

<section>
    <h3>Строки</h3>
    <span>Строка в PHP - это набор символов любой длины. В отличие от Си, строки могут содержать в себе также и нулевые символы, что никак не повлияет на программу.
        Иными словами, строки можно использовать для хранения бинарных данных. Длина строки ограничена только размером свободой оперативной памяти.</span>
        <pre>
           <?php
           highlight_string('
<?
$a = "Это просто текст, записанный в строковую переменную";
echo $a; //Выводит :"Это просто текст, записанный в строковую переменную"
?>
           ');
           ?>
        </pre>
</section>


<section>
    <h3>Синтаксис типа string (строк)</h3>
    <p>Строка может быть определена тремя различными способами.</p>
    <ul>
       <li class="fragment"> одинарными кавычками
        <li class="fragment">  двойными кавычками
        <li class="fragment"> heredoc-синтаксисом
        </ul>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
echo \'это простая строка\';
echo "это простая строка";
echo <<<EOT
Меня зовут "$name". Я печатаю $foo->foo.
Теперь я вывожу {$foo->bar[1]}.
EOT;
?>
        ');
        ?>
        </pre>
</section>

<section>
    <h3>Массивы (Array)</h3>
    <p>Массивы (arrays) - это упорядоченные наборы данных, представляющие собой список однотипных элементов.</p>
    <ol>
        <li class="fragment"> В массивах первого типа элемент определяется индексом в последовательности. Такие массивы называются простыми массивами.
        <li class="fragment"> Массивы второго типа имеют ассоциативную природу, и для обращения к элементам используются ключи, логически связанные со значениями. Такие массивы называют ассоциативными массивами.
    </ol>
</section>

<section>
    <h3>Простые массивы и списки в PHP</h3>
    <p>При обращении к элементам простых индексируемых массивов используется целочисленный индекс, определяющий позицию заданного элемента.</p>
    <pre class="fragment">
        <?php
        highlight_string('
$имя[индекс];
<?php
// Простой способ инициализации массива
$names[0]="Апельсин";
$names[1]="Банан";
$names[2]="Груша";
$names[3]="Помидор";
// Здесь: names - имя массива, а 0, 1, 2, 3 - индексы массива

/ Выводим элементы массивов в браузер:
echo $names[0]; // Вывод элемента массива names с индексом 0
echo $names[3]; // Вывод элемента массива names с индексом 3
// Выводит:
// Апельсин
// Помидор
?>
        ');
        ?>
        </pre>
</section>

<section>
    <p>С технической точки зрения разницы между простыми массивами и списками нет.
        Простые массивы можно создавать, не указывая индекс нового элемента массива, это за вас сделает PHP. Вот пример:</p>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
// Простой способ инициализации массива, без указания индексов
$names[]="Апельсин";
$names[]="Банан";
$names[]="Груша";
$names[]="Помидор";
// PHP автоматически присвоит индексы элементам массива, начиная с 0

// Выводим элементы массивов в браузер:
echo $names[0]; // Вывод элемента массива names с индексом 0
echo "<br>";
echo $names[3]; // Вывод элемента массива names с индексом 3
// Выводит:
// Апельсин
// Помидор
?>
        ');
        ?>
        </pre>
</section>

<section>
    <p>Пример простого многомерного массива:</p>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
// Многомерный простой массив:
$arr[0][0]="Овощи";
$arr[0][1]="Фрукты";
$arr[1][0]="Абрикос";
$arr[1][1]="Апельсин";
$arr[1][2]="Банан";
$arr[2][0]="Огурец";
$arr[2][1]="Помидор";
$arr[2][2]="Тыква";

// Выводим элементы массива:
echo "<h3>".$arr[0][0].":</h3>";
for ($q=0; $q<=2; $q++) {
echo $arr[2][$q]."<br>";
}
echo "<h3>".$arr[0][1].":</h3>";
for ($w=0; $w<=2; $w++) {
echo $arr[1][$w]."<br>";
}
?>
        ');
        ?>
        </pre>
</section>

<section>
    <p>Ассоциативные массивы в PHP</p>
    <p>В PHP индексом массива может быть не только число, но и строка. Причем на такую строку не накладываются никакие ограничения: она может содержать пробелы, длина такой строки может быть любой.
        Ассоциативные массивы особенно удобны в ситуациях, когда элементы массива удобнее связывать со словами, а не с числами.</p>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
// Ассоциативный массив
$names["Иванов"]="Иван";
$names["Сидоров"]="Николай";
$names["Петров"]="Петр";
// В данном примере: фамилии - ключи ассоциативного массива
// , а имена - элементы массива names
?>
        ');
        ?>
        </pre>
</section>

<section>
    <p>Многомерные ассоциативные массивы:</p>
    <p>Многомерные ассоциативные массивы могут содержать несколько ключей, соответствующих конкретному индексу ассоциативного массива. Рассмотрим пример многомерного ассоциативного массива:</p>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
// Многомерный массив
$A["Ivanov"] = array("name"=>"Иванов И.И.", "age"=>"25", "email"=>"ivanov@mail.ru");
$A["Petrov"] = array("name"=>"Петров П.П.", "age"=>"34", "email"=>"petrov@mail.ru");
$A["Sidorov"] = array("name"=>"Сидоров С.С.", "age"=>"47", "email"=>"sidorov@mail.ru");
?>

echo $A["Ivanov"]["name"]; // Выводит Иванов И.И.
echo $A["Petrov"]["email"]; // Выводит petrov@mail.ru
        ');
        ?>
        </pre>
</section>

<section>
    <h3>Функции для работы с массивами</h3>
    <p class="fragment highlight-green">просмотреть самостоятельно</p>
</section>

<section>
    <p>Объекты (Object)</p>
    Объект является одним из базовых понятий объектно-ориентированного программирования.
    Объект - это совокупность данных (свойств) и функций (методов) для их обработки. Данные и методы называются членами класса. Вообще, объектом является все то, что поддерживает инкапсуляцию.
    Для инициализации объекта используется выражение new, создающее в переменной экземпляр объекта.

</section>

<section>
        <pre>
        <?php
        highlight_string('
<?php
class foo
{
     function do_foo()
     {
         echo "Doing foo.";
     }
}

$bar = new foo;
$bar->do_foo();
?>
        ');
        ?>
        </pre>
    </section>

<section>
    <p>Специальный тип Resource (ресурсы)</p>
    Ресурс - это специальная переменная, содержащая ссылку на внешний ресурс. Ресурсы создаются и используются специальными функциями.
    Поскольку тип ресурс содержит специальные указатели на открытые файлы, соединения с базой данных,
    область изображения и тому подобное, вы не можете преобразовать какое-либо значение в ресурс.

</section>

<section>
    <h3>Специальный тип Null (пустой тип)</h3>
    Специальное значение NULL говорит о том, что эта переменная не имеет значения. NULL - это единственно возможное значение типа NULL.
    <ul>Переменная считается NULL если
        <li class="fragment"> ей была присвоена константа NULL.
        <li class="fragment">  ей еще не было присвоено какое-либо значение.
        <li class="fragment">  она была удалена с помощью unset().
        </ul>
</section>

<section>
    <p>Псевдотип Mixed (смешанный)</p>
    Mixed говорит о том, что параметр может принимать множество (но не обязательно все) типов.

    Это может быть целое или дробное число, строка, массив или объект...
    Например, параметр типа mixed имеет стандартная функция gettype() или функция settype().
    Если написано, что функция возвращает mixed, это значит, что тип результата зависит от операндов и уточняется при описании функции.

</section>

<section>
    Псевдотип Number (числа)
    Псевдотип number говорит о том, что параметр может быть либо integer, либо float.
</section>

<section>
    <h3>Псевдотип Callback (обратный вызов)</h3>
    Некоторые функции, такие как call_user_func() или usort() принимают в качестве параметра определенные пользователем callback-функции.
    Callback-функции могут быть не только простыми функциями, но также методами объектов, включая статические методы классов.

    PHP-функция передается просто как строка ее имени.
    Вы можете передать любую встроенную или определенную пользователем функцию за исключением array(), echo(), empty(), eval(), exit(), isset(), list(), print() и unset().

</section>

<section>
    Примеры callback-функций:
            <pre>
        <?php
        highlight_string('
<?php

// простой пример callback
function my_callback_function() {
     echo \'hello world!\';
}
call_user_func(\'my_callback_function\');

// примеры callback-метода
class MyClass {
     function myCallbackMethod() {
         echo \'Hello World!\';
     }
}

// вызов метода статического класса без создания объекта
call_user_func(array(\'MyClass\', \'myCallbackMethod\'));

// вызов метода объекта
$obj = new MyClass();
call_user_func(array(&$obj, \'myCallbackMethod\'));
?>
        ');
        ?>
        </pre>
    </section>

<section>
    <h3>Манипуляции с типами данных PHP</h3>
    PHP не требует (и не поддерживает) явного определения типа при объявлении переменной;
    тип переменной определяется согласно контексту, в котором она используется.
    То есть, если вы присвоите строковое значение переменной $var, $var станет строкой.
    Если вы затем присвоите $var целочисленное значение, она станет целым числом.

</section>


<section>
            <pre>
        <?php
        highlight_string('
<?php
$foo = "0";  // $foo это строка (ASCII 48)
$foo += 2;   // $foo теперь целое число (2)
$foo = $foo + 1.3;  // $foo теперь число с плавающей точкой (3.3)
$foo = 5 + "10 Little Piggies"; // $foo это целое число (15)
$foo = 5 + "10 Small Pigs";     // $foo это целое число (15)
?>
        ');
        ?>
        </pre>
</section>

<section>
    <h3>Приведение типов</h3>
    Приведение типов в PHP работает так же, как и в C: имя требуемого типа записывается в круглых скобках перед приводимой переменной. Пример:
            <pre>
        <?php
        highlight_string('
<?php
$foo = 10;   // $foo это целое число
$bar = (boolean) $foo;   // $bar это булев тип
?>
        ');
        ?>
        </pre>
</section>

<section>
    <ul>Допускаются следующие приведения типов:
       <li class="fragment"> (int), (integer) - приведение к целому числу

        <li class="fragment"> (bool), (boolean) - приведение к булеву типу

        <li class="fragment"> (float), (double), (real) - приведение к числу с плавающей точкой (float)

        <li class="fragment"> (string) - приведение к строке

        <li class="fragment"> (array) - приведение к массиву

        <li class="fragment"> (object) - приведение к объекту
        </ul>
    </section>

<section>
    <h3>Выражения в PHP</h3>
    Основными формами выражений являются константы и переменные.
            <pre>
        <?php
        highlight_string('
//Например, если вы записываете "$a = 100", вы присваиваете \'100\' переменной $a:
$a = 100;
//В приведенном примере $a - это переменная, = - это оператор присваивания, а 100 - это и есть выражения. Его значение 100.
        ');
        ?>
        </pre>
    Выражением может быть и переменная, если ей сопоставлено определенное значение:
                <pre>
        <?php
        highlight_string('
$x = 7;
$y = $x;
        ');
        ?>
        </pre>
</section>

<section>
    <h3>Операторы PHP</h3>
    Оператором называется нечто, состоящее из одного или более значений (выражений, если говорить на жаргоне программирования),
    которое можно вычислить как новое значение (таким образом, вся конструкция может рассматриваться как выражение).
</section>

<section>
    <h3>Операторы присвоения:</h3>
                    <pre>
        <?php
        highlight_string('
<?php

$a = ($b = 4) + 5; // результат: $a установлена значением 9, переменной $b присвоено 4.

?>
        ');
        ?>
        </pre>
    </section>

<section>
    <h3>Комбинированные операторы:</h3>
                    <pre>
        <?php
        highlight_string('
<?php

$a = 3;
$a += 5; // устанавливает $a значением 8, аналогично записи: $a = $a + 5;
$b = "Hello ";
$b .= "There!"; // устанавливает $b строкой "Hello There!",  как и $b = $b . "There!";

?>
        ');
        ?>
        </pre>
</section>

<section>
    <h3>Строковые операторы:</h3>
                    <pre>
        <?php
        highlight_string('
<?php
$a = "Hello ";
$b = $a . "World!"; // $b содержит строку "Hello World!"

$a = "Hello ";
$a .= "World!";     // $a содержит строку "Hello World!"
?>
        ');
        ?>
        </pre>
</section>
<section>
<h3>Управляющие конструкции языка PHP</h3>

<ul>Основными конструкциями языка PHP являются:

<li class="fragment"> Условные операторы (if, else);
    <li class="fragment"> Циклы (while, do-while, for, foreach, break, continue);
    <li class="fragment"> Конструкции выбора (switch);
    <li class="fragment"> Конструкции объявления (declare);
    <li class="fragment"> Конструкции возврата значений (return);
    <li class="fragment"> Конструкции включений (require, include).
   </ul>
    </section>

<section>
    В PHP существуют шесть основных групп управляющих конструкций.
    <ul class="fragment">» Условные операторы:
        <li class="fragment"> if
        <li class="fragment"> else
        <li class="fragment"> elseif
        </ul>
</section>

<section>
    <ul>Циклы
        <li class="fragment"> while
        <li class="fragment"> do-while
        <li class="fragment"> for
        <li class="fragment">  foreach
        <li class="fragment">  break
        <li class="fragment">  continue
    </ul>
</section>

<section>
<ul>» Конструкции выбора:
    <li class="fragment"> switch-case
</ul>
    <ul class="fragment">» Конструкции объявления:

<li class="fragment">declare
</ul>

    <ul class="fragment">» Конструкции возврата значений:

        <li class="fragment">return
    </ul>

    <ul class="fragment">
        » Конструкции включений:

        <li class="fragment"> require()
        <li class="fragment"> include()
        <li class="fragment"> require_once()
        <li class="fragment"> include_once()
        </ul>
    </section>

<section>
    <h3>Пользовательские функции в PHP</h3>
    В любом языке программирования существуют подпрограммы.
    В языке C они называются функциями, в ассемблере - подпрограммами, а в Pascal существуют два вида подпрограмм: процедуры и функции.

    В PHP такими подпрограммами являются пользовательские функции.

    Подпрограмма - это специальным образом оформленный фрагмент программы, к которому можно обратиться из любого места внутри программы.
    Подпрограммы существенно упрощают жизнь программистам,
    улучшая читабельность исходного кода, а также сокращая его, поскольку отдельные фрагменты кода не нужно писать несколько раз.
    </section>
<section>
    <h3>Пример пользовательской функции:</h3>
    <pre>
        <?php
        highlight_string('
<?php

function funct() {
$a = 100;
 echo "<h4>$a</h4>";
}
funct();

?>
        ');
        ?>
        </pre>
    </section>

<section>
    <h3>Встроенные (стандартные) функции PHP</h3>
   <pre>
<?php
highlight_string('
    <?php
    $foo = \'hello world!\';
    $foo = ucwords($foo);             // Hello World!

    $bar = \'HELLO WORLD!\';
    $bar = ucwords($bar);             // HELLO WORLD!
    $bar = ucwords(strtolower($bar)); // Hello World!
    ?>
     ');
        ?>
        </pre>
</section>

<section>
    <h3>ООП и PHP</h3>
    PHP имеет достаточно хорошую поддержку объектно-ориентированного программирования (ООП).

    В PHP можно создавать классы различных уровней, объекты и достаточно гибко ими оперировать.
    <pre>
    <?php
    highlight_string('
    <?php
// Создаем новый класс Coor:
class Coor {
// данные (свойства):
var $name;

// методы:
 function Getname() {
 echo "<h3>John</h3>";
 }

}

// Создаем объект класса Coor:
$object = new Coor;
// Получаем доступ к членам класса:
$object->name = "Alex";
echo $object->name;
// Выводит \'Alex\'
// А теперь получим доступ к методу класса (фактически, к функции внутри класса):
$object->Getname();
// Выводит \'John\' крупными буквами
?>
    ');
    ?>
    </pre>
</section>

<section>
    <h3>Задание:</h3>
    Разработать небольшую систему авторизации и проверки прав доступа
    <ul class="fragment"> Составляющие системы:
        <li class="fragment"> массив пользовательских данных (логин, пароль, mail по-желанию)
        <li class="fragment"> массив ролей пользователей с правами доступа
        <li class="fragment"> форма авторизации пользователя
        <li class="fragment"> страница пользователя с информацией о нем и о его правах доступа
    </ul>
    </section>

<section>
    <pre>
<?php
    highlight_string('
return array(
    \'user\'=>array(
            \'type\'=>\'role\',
            \'description\'=>\'typical user\',
            \'children\'=>\'array()\',
            \'actions\'=>\'array(\'login\',\'logout\',\'index\')\',
        ),
    \'moderator\'=>array(
            \'type\'=>\'role\',
            \'description\'=>\'user with ability to edit content\',
            \'children\'=>\'array(\'user\')\',
            \'actions\'=>\'array(\'edit\')\',
        ),
);
    ');
?>
    </pre>
</section>

