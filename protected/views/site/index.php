<div class="reveal">

<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">

<section>
    <h1>HTML & CSS</h1>
    <p>
    <ul>
        <li class="fragment">HTML - hypertext markup language</li>
        <span class="fragment">(язык гипертекстовой разметки)</span><br />
        <span class="fragment"><strong>HTML - это язык разметки, но не язык программирования!</strong></span>
        <li class="fragment">CSS - cascading stylesheets</li>
        <span class="fragment">(каскадные таблицы стилей)</span>
    </ul>
    </p>
</section>

<section>
    <h2>Для чего нужен HTML?</h2>
    <ul>
        <li class="fragment">Чтобы создавать веб-страницы (документы .html)</li>
        <li class="fragment">Создавать разметку страниц</li>
        <li class="fragment">Создавать страницы с разметкой, которые корректно отображаются в браузерах</li>
    </ul>
</section>

<section>
    <h2>Для чего нужен CSS?</h2>
</section>

<section>
    <h3>Чтобы никогда не создавать подобные сайты!</h3>
    <img src="/images/worst_design.jpg" />
</section>

<section>
    <h3>Структура html-документа</h3>
    <img src="/images/html_basestruct.jpg" />
</section>

<section>
    <h3>Структура html-тега</h3>
    <img src="/images/tag_structure.jpg" />
</section>

<section>
    <h3>Условно теги делятся на следующие типы:</h3>
    <ul>
        <li class="fragment">теги верхнего уровня;
        <li class="fragment">теги заголовка документа;
        <li class="fragment">блочные элементы;
        <li class="fragment">строчные элементы;
        <li class="fragment">списки;
        <li class="fragment">таблицы.
    </ul>
</section>

<section>
    <h3>теги верхнего уровня</h3>
    <ul>
        <li class="fragment">html
        <li class="fragment">head
        <li class="fragment">body
    </ul>
</section>

<section>
    <h3>теги заголовка документа</h3>
    <ul class="fragment">
        <li class="fragment">title
        <li class="fragment">meta
    </ul>
</section>
<section>
    <h3>блочные элементы</h3>
    <ul>
        <li class="fragment">blockquote
        <li class="fragment">h1 - h6
        <li class="fragment">hr
        <li class="fragment">pre
        <li class="fragment">p
    </ul>
</section>
<section>
    <h3>строчные элементы</h3>
    <ul>
        <li class="fragment">a
        <li class="fragment">br
        <li class="fragment">em
        <li class="fragment">img
        <li class="fragment">span
    </ul>
</section>
<section>
    <h3>списки</h3>
    <ul>
        <li class="fragment">ol
        <li class="fragment">ul
        <li class="fragment">li
    </ul>
</section>
<section>
    <h3>таблицы</h3>
    <ul>
        <li class="fragment">table
        <li class="fragment">thead
        <li class="fragment">tbody
        <li class="fragment">tr
        <li class="fragment">td
    </ul>
    </ul>
</section>

<section>
    <h3>Атрибуты тегов</h3>
    <p>Атрибуты тегов расширяют возможности самих тегов и позволяют гибко управлять различными настройками отображения элементов веб-страницы.</p>
    <ul>
        <li class="fragment">Цвет
        <li class="fragment">Размер
        <li class="fragment">Адрес
    </ul>
</section>

<section>
    <h3>Работа с текстом</h3>
    <ul>
        <li class="fragment">Абзацы
        <li class="fragment">Заголовки
        <li class="fragment">Выравнивание текста
        <li class="fragment">Начертание
        <li class="fragment">Верхний и нижний индексы (sub, sup)
        <li class="fragment">Спецсимволы (&copy;)
    </ul>
</section>

<section>
    <h3>Основные виды верстки:</h3>
    <h4 class="fragment">Табличная верстка</h4>
    <ul>
        <li class="fragment">Долго грузится</li>
        <li class="fragment">Большой обьём кода (table,thead,tbody,tr,td,th ... )</li>
        <li class="fragment">Сложно создать оригинальный дизайн</li>
        <li class="fragment">Разброс информации, плохая индексация в поисковиках</li>
    </ul>
    <h4 class="fragment">Блочная верстка</h4>
    <ul>
        <li class="fragment">Гораздо быстрее грузится</li>
        <li class="fragment">Меньше кода</li>
        <li class="fragment">Позволяет создать оригинальный дизайн</li>
        <li class="fragment">Более сложная верстка, особенно при изменениях размеров екрана</li>
    </ul>
</section>

<section>
    <h1>MArgins & Paddings</h1>
    <img src="images/margins_paddings.png" />
</section>


<!-- Example of nested vertical slides -->
<!--				<section>-->
<!--					<section>-->
<!--						<h2>Vertical Slides</h2>-->
<!--						<p>-->
<!--							Slides can be nested inside of other slides,-->
<!--							try pressing <a href="#" class="navigate-down">down</a>.-->
<!--						</p>-->
<!--						<a href="#" class="image navigate-down">-->
<!--							<img width="178" height="238" src="https://s3.amazonaws.com/hakim-static/reveal-js/arrow.png" alt="Down arrow">-->
<!--						</a>-->
<!--					</section>-->
<!--					<section>-->
<!--						<h2>Basement Level 1</h2>-->
<!--						<p>Press down or up to navigate.</p>-->
<!--					</section>-->
<!--					<section>-->
<!--						<h2>Basement Level 2</h2>-->
<!--						<p>Cornify</p>-->
<!--						<a class="test" href="http://cornify.com">-->
<!--							<img width="280" height="326" src="https://s3.amazonaws.com/hakim-static/reveal-js/cornify.gif" alt="Unicorn">-->
<!--						</a>-->
<!--					</section>-->
<!--					<section>-->
<!--						<h2>Basement Level 3</h2>-->
<!--						<p>That's it, time to go back up.</p>-->
<!--						<a href="#/2" class="image">-->
<!--							<img width="178" height="238" src="https://s3.amazonaws.com/hakim-static/reveal-js/arrow.png" alt="Up arrow" style="-webkit-transform: rotate(180deg);">-->
<!--						</a>-->
<!--					</section>-->
<!--				</section>-->


<section data-markdown>
    <script type="text/template">
        ```
        body {
        font-family: Arial, Verdana,  sans-serif; /* Семейство шрифтов */
        font-size: 11pt; /* Размер основного шрифта в пунктах  */
        background-color: #f0f0f0; /* Цвет фона веб-страницы */
        color: #333; /* Цвет основного текста */
        }
        h1 {
        color: #a52a2a; /* Цвет заголовка */
        font-size: 24pt; /* Размер шрифта в пунктах */
        font-family: Georgia, Times, serif; /* Семейство шрифтов */
        font-weight: normal; /* Нормальное начертание текста  */
        }
        p {
        text-align: justify; /* Выравнивание по ширине */
        margin-left: 60px; /* Отступ слева в пикселах */
        margin-right: 10px; /* Отступ справа в пикселах */
        border-left: 1px solid #999; /* Параметры линии слева */
        border-bottom: 1px solid #999; /* Параметры линии снизу */
        padding-left: 10px; /* Отступ от линии слева до текста  */
        padding-bottom: 10px; /* Отступ от линии снизу до текста  */
        }
        ```
    </script>
</section>

<section data-markdown>
<script type="text/template">
```
<link rel="stylesheet" type="text/css" href="mysite.css">
```
```
                        <pre>
                        <style type="text/css">
                            H1 {
                            font-size: 120%;
                            font-family: Verdana, Arial, Helvetica, sans-serif;
                            color: #333366;
                            }
                        </style>
                        </pre>
                        ```
                        ```
                        H1 {
                              color: #000080;
                              font-size: 200%;
                              font-family: Arial, Verdana, sans-serif;
                              text-align: center;
                          }
                        P {
                            padding-left: 20px;
                        }
                        ```
                         ```
                         <p style="font-size: 120%; font-family: monospace; color: #cd66cc">Пример текста</p>
                        ```
                    </script>
                </section>

                <section data-markdown>
                    <script type="text/template">
                        ```
                          P {
                            text-align: justify; /* Выравнивание по ширине */
                            color: green; /* Зеленый цвет текста */
                        }
                        ```

                        ```
                         P.cite { /* Абзац с классом cite */
                            color: navy; /* Цвет текста */
                            margin-left: 20px; /* Отступ слева */
                            border-left: 1px solid navy; /* Граница слева от текста */
                            padding-left: 15px; /* Расстояние от линии до текста */
                        }
                        ```

                        ```
                         #help {
                            position: absolute; /* Абсолютное позиционирование */
                            left: 160px; /* Положение элемента от левого края */
                            top: 50px; /* Положение от верхнего края */
                            width: 225px; /* Ширина блока */
                            padding: 5px; /* Поля вокруг текста */
                            background: #f0f0f0; /* Цвет фона */
                        }
                        ```
                        ```
                        <title>Универсальный селектор</title>
                        <style type="text/css">
                        * {
                            font-family: Arial, Verdana, sans-serif; /* Рубленый шрифт для текста */
                            font-size: 96%; /* Размер текста */
                        }
                        ```
                    </script>
                    </section>


				<section id="transitions">
                        <h2>Transition Styles</h2>
					<p>
						You can select from different transitions, like: <br>
						<a href="?transition=cube#/transitions">Cube</a> -
                        <a href="?transition=page#/transitions">Page</a> -
                        <a href="?transition=concave#/transitions">Concave</a> -
                        <a href="?transition=zoom#/transitions">Zoom</a> -
                        <a href="?transition=linear#/transitions">Linear</a> -
                        <a href="?transition=fade#/transitions">Fade</a> -
                        <a href="?transition=none#/transitions">None</a> -
                        <a href="?#/transitions">Default</a>
					</p>
				</section>

				<section id="themes">
                        <h2>Themes</h2>
					<p>
						Reveal.js comes with a few themes built in: <br>
						<a href="?theme=sky#/themes">Sky</a> -
                        <a href="?theme=beige#/themes">Beige</a> -
                        <a href="?theme=simple#/themes">Simple</a> -
                        <a href="?theme=serif#/themes">Serif</a> -
                        <a href="?theme=night#/themes">Night</a> -
                        <a href="?#/themes">Default</a>
					</p>
					<p>
						<small>
							* Theme demos are loaded after the presentation which leads to flicker. In production you should load your theme in the <code>&lt;head&gt;</code> using a <code>&lt;link&gt;</code>.
						</small>
					</p>
				</section>

				<section>
					<section data-state="alert">
                        <h2>Global State</h2>
						<p>
							Set <code>data-state="something"</code> on a slide and <code>"something"</code>
							will be added as a class to the document element when the slide is open. This lets you
							apply broader style changes, like switching the background.
						</p>
						<a href="#" class="image navigate-down">
                        <img width="178" height="238" src="https://s3.amazonaws.com/hakim-static/reveal-js/arrow.png" alt="Down arrow">
                        </a>
					</section>
					<section data-state="blackout">
                        <h2>"blackout"</h2>
						<a href="#" class="image navigate-down">
                        <img width="178" height="238" src="https://s3.amazonaws.com/hakim-static/reveal-js/arrow.png" alt="Down arrow">
                        </a>
					</section>
					<section data-state="soothe">
                        <h2>"soothe"</h2>
						<a href="#" class="image navigate-next">
                        <img width="178" height="238" src="https://s3.amazonaws.com/hakim-static/reveal-js/arrow.png" alt="Up arrow" style="-webkit-transform: rotate(-90deg);">
                        </a>
					</section>
				</section>

				<section data-state="customevent">
                        <h2>Custom Events</h2>
					<p>
						Additionally custom events can be triggered on a per slide basis by binding to the <code>data-state</code> name.
					</p>
					<pre><code data-trim contenteditable style="font-size: 18px; margin-top: 20px;">
                        Reveal.addEventListener( 'customevent', function() {
                            console.log( '"customevent" has fired' );
                        } );
                        </code></pre>
				</section>

				<section>
					<h2>Clever Quotes</h2>
					<p>
						These guys come in two forms, inline: <q cite="http://searchservervirtualization.techtarget.com/definition/Our-Favorite-Technology-Quotations">
                        &ldquo;The nice thing about standards is that there are so many to choose from&rdquo;</q> and block:
					</p>
					<blockquote cite="http://searchservervirtualization.techtarget.com/definition/Our-Favorite-Technology-Quotations">
                        &ldquo;For years there has been a theory that millions of monkeys typing at random on millions of typewriters would
						reproduce the entire works of Shakespeare. The Internet has proven this theory to be untrue.&rdquo;
                        </blockquote>
				</section>

				<section>
					<h2>Pretty Code</h2>
					<pre><code data-trim contenteditable>
function linkify( selector ) {
                        if( supports3DTransforms ) {

                            var nodes = document.querySelectorAll( selector );

                        for( var i = 0, len = nodes.length; i &lt; len; i++ ) {
                            var node = nodes[i];

                        if( !node.className ) ) {
                            node.className += ' roll';
                        }
                            };
                            }
                            }
                        </code></pre>
<pre>
    <code data-trim class="no-highlight">
                        <?php echo "";?>
                        </code>
</pre>

					<p>Courtesy of <a href="http://softwaremaniacs.org/soft/highlight/en/description/">highlight.js</a>.</p>
				</section>

				<section>
					<h2>Intergalactic Interconnections</h2>
					<p>
						You can link between slides internally,
                                                                                                                                                                                                                                <a href="#/2/3">like this</a>.
					</p>
				</section>

				<section>
					<section>
						<h2>Fragmented Views</h2>
						<p>Hit the next arrow...</p>
						<p class="fragment">... to step through ...</p>
						<ol>
							<li class="fragment"><code>any type</code></li>
							<li class="fragment"><em>of view</em></li>
							<li class="fragment"><strong>fragments</strong></li>
						</ol>

						<aside class="notes">
                        This slide has fragments which are also stepped through in the notes window.
						</aside>
					</section>
					<section>
						<h2>Fragment Styles</h2>
						<p>There's a few styles of fragments, like:</p>
                        <p class="fragment grow">grow</p>
						<p class="fragment shrink">shrink</p>
						<p class="fragment roll-in">roll-in</p>
						<p class="fragment fade-out">fade-out</p>
						<p class="fragment highlight-red">highlight-red</p>
						<p class="fragment highlight-green">highlight-green</p>
						<p class="fragment highlight-blue">highlight-blue</p>
					</section>
				</section>

				<section>
					<h2>Spectacular image!</h2>
					<a class="image" href="http://lab.hakim.se/meny/" target="_blank">
                        <img width="320" height="299" src="http://s3.amazonaws.com/hakim-static/portfolio/images/meny.png" alt="Meny">
                        </a>
				</section>

				<section>
					<h2>Export to PDF</h2>
					<p>Presentations can be <a href="https://github.com/hakimel/reveal.js#pdf-export">exported to PDF</a>, below is an example that's been uploaded to SlideShare.</p>
                        <iframe id="slideshare" src="http://www.slideshare.net/slideshow/embed_code/13872948" width="455" height="356" style="margin:0;overflow:hidden;border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen> </iframe>
					<script>
						document.getElementById('slideshare').attributeName = 'allowfullscreen';
                        </script>
				</section>

				<section>
					<h2>Take a Moment</h2>
					<p>
						Press b or period on your keyboard to enter the 'paused' mode. This mode is helpful when you want to take distracting slides off the screen
						during a presentation.
					</p>
				</section>

				<section>
					<h2>Stellar Links</h2>
					<ul>
						<li><a href="http://slid.es">Try the online editor</a></li>
						<li><a href="https://github.com/hakimel/reveal.js">Source code on GitHub</a></li>
						<li><a href="http://twitter.com/hakimel">Follow me on Twitter</a></li>
					</ul>
				</section>

				<section>
					<h1>THE END</h1>
					<h3>BY Hakim El Hattab / hakim.se</h3>
				</section>

			</div>

		</div>




