<?php
    $this->pageTitle = "Yii :: working with views";
?>
<div class="reveal">
<img src="/images/Yii-logo-transparent.png" />
<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">

    <section>
        <h3>Views - вьюшки =) (отображения)</h3>
        <ul class="fragment">Что нужно знать о view ?
            <li class="fragment">Рендерятся контроллером
                <pre>
                    <?php highlight_string('
$this->render("viewName",array(
    "param"=>$param,
    // .... другие параметры
));
                    ');?>
                </pre>
            </li>

            <li class="fragment">Расположение - /protected/views</li>
            <li class="fragment">Рисуются с учетом заданного шаблона</li>
        </ul>
    </section>

    <section>
        <h3>Шаблон, template, layout</h3>
        <small>native php-style</small>
        <pre>
            <?php highlight_string('
<?php
    include "header.php";
    echo $content;
    include "footer.php";
            ');?>
        </pre>
    </section>

    <section>
        <h3>Yii-way layouts</h3>
        <ul>3-х колоночный макет:</ul>
            <li>/protected/views/layouts</li>
            <ul>
                <li class="fragment">main.php</li>
                <li class="fragment">column1.php</li>
                <li class="fragment">column2.php</li>
            </ul>
    </section>

    <section>
        <h3>Yii layout by Larry (simple)</h3>
        <img src="/images/yii_layout_larry.png" />
    </section>

    <section>
        <h3>Yii layout flow</h3>
        <img src="/images/yii_layout_flow.png" />
    </section>

    <section>
        <h3>В чем подвох ?</h3>
        <p class="fragment">Вы нигде не найдете обьявления $content!</p>
        <p class="fragment">Нужно иметь ввиду, что $content это и есть содержимое подставляемое в шаблон (то, что мы отрендерим)</p>
    </section>

    <section>
        <h3>Работа с отображениями:</h3>
        <ul>
            <li class="fragment">Нужно знать, понимать какие переменные и как попадают во view и передавать их
                (все "генерится" контроллером, вью только отображает &copy MVC)</li>
            <li class="fragment">$this = контекст контроллера!</li>
            <li class="fragment">Можем задать тайтл страницы</li>
            <li class="fragment">Можем создавать ссылки на другие страницы (внутренние)</li>
            <li class="fragment">Можем создавать ссылки на другие страницы (внешние, абсолютные)</li>
            <li class="fragment">Можем использовать формы, виджеты, клипы (вспомогательные элементы)</li>
        </ul>
    </section>

    <section>
        <img src="/images/tip.png" />
        <p>view, как и контроллеры важно документировать!</p>
        <pre>
<?php
highlight_string('
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
');
?>
        </pre>

    </section>

    <section>
        <h3>Передача параметров во view</h3>
        <ul>
            <li class="fragment">$_GET, $_POST = бить по рукам =)</li>
            <pre class="fragment">
<?php
highlight_string('
$name = "Alexei";
$lastName = "Smolyanov";
$fathersName = "Bronislavovich";

$array = array($name,$lastName,$fathersName);

$this->render("index",array(
    "name"=>$name,
    "lastName"=>$lastame,
    "fName"=>$fathersName,
    "data"=>$array,
);
');
?>
            </pre>
            <li class="fragment">установить $this->pageTitle</li>
        </ul>
    </section>

    <section>
        <h3>Практика:</h3>
        <img src="/images/men_at_work.png"  style="width: 400px"/>

        <ul>
            <li class="fragment">посмотреть CRUD-страницы, обратить внимание на layouts,widgets</li>
            <li class="fragment">натянуть свой стиль на layout</li>
            <li class="fragment">подключить компонент yii-booster</li>
        </ul>
    </section>

</div>
</div>
