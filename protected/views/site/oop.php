<div class="reveal">

<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">

<section>
    <h3>PHP OOP</h3>
    <ul class="fragment">Объектно-ориентированное программирование основано на:
        <li class="fragment">Инкапсуляции</li>
        <li class="fragment">Полиморфизме</li>
        <li class="fragment">Наследовании</li>
    </ul>
</section>

    <section>
        <h3>Инкапсуляция</h3>
        Инкапсуляция - это механизм, объединяющий данные и обрабатывающий их код как единое целое.
        Многие подробности "внутреннего устройства" скрываются от пользователя, что позволяет ему сосредоточиться на решении конкретных задач.
        В ООП эта возможность обеспечивается классами, объектами и различными средствами выражения иерархических связей между ними.
    </section>

    <section>
        <h3>Полиморфизм</h3>
        Полиморфизм позволяет использовать одни и те же имена для похожих, но технически разных задач.
        Главным в полиморфизме является то, что он позволяет манипулировать объектами путем создания стандартных интерфейсов для схожих действий.
    </section>

    <section>
        <h3>Наследование</h3>
        Наследование позволяет одному объекту приобретать свойства другого объекта.
        При копировании создается точная копия объекта,
        а при наследовании точная копия дополняется уникальными свойствами, которые характерны только для производного объекта.
    </section>

    <section>
        <h3>Классы</h3>
        Класс - это базовое понятие в объектно-ориентированном программировании (ООП).
        Классы образуют синтаксическую базу ООП. Их можно рассматривать как своего рода "контейнеры" для логически связанных данных и функций
    </section>

    <section>
        <h3>Объявление класса</h3>
        <ul>
            <li class="fragment">ключевое слово "class"</li>
            <li class="fragment">имя класса</li>
            <li class="fragment">тело класса</li>
        </ul>
        <pre class="fragment">
            <?php
                highlight_string('
<?php
class ParentClass
{

}
?>
                ');
            ?>
        </pre>
    </section>

    <section>
        <h3>Свойства класса</h3>
        <ul>
            <li class="fragment">модификатор доступа</li>
            <li class="fragment">имя свойства</li>
            <li class="fragment">значение по умолчанию (если есть)</li>
        </ul>
        <pre class="fragment">
            <?php
            highlight_string('
<?php
class ParentClass
{
    public $someOption = null;
    protected $someNewOption = null;
    private $someElseOption = null;
}
?>
            ');
            ?>
            </pre>
    </section>

    <section>
        <h3>Методы класса</h3>
        <ul>
            <li class="fragment">модификатор доступа</li>
            <li class="fragment">имя метода</li>
            <li class="fragment">тело метода</li>
        </ul>
        <pre class="fragment">
            <?php
            highlight_string('
<?php
class ParentClass
{
    public $myName = "lesha";

    public function getMyName()
    {
        echo $this->myName;
    }
}
?>
            ');
            ?>
            </pre>
    </section>


    <section>
        <h3>Доступ к свойствам и методам класса</h3>

        <h3>указатель $this</h3>
        <pre class="fragment">
            <?php
            highlight_string('
<?php
class ParentClass
{
    public $myName = "lesha";

    public function getMyName()
    {
        return $this->myName;
    }
}
?>
            ');
            ?>
            </pre>
    </section>

    <section>
        <h3>Объект</h3>
        <ul>
            <li class="fragment">Если класс это шаблон, то обьект это ..?</li>
            <li class="fragment">Экземпляр класса - это объект. Объект - это совокупность данных (свойств) и функций (методов) для их обработки.</li>
        </ul>
        <pre class="fragment">
            <?php
            highlight_string('
<?php
    $object = new ParentClass;
?>
            ');
            ?>
            </pre>
        <p class="fragment">Ошибка?</p>
    </section>

    <section>
        <pre>
            <?php
            highlight_string('
<?php
  include "ParentClass.php";

    $object = new ParentClass;
?>
            ');
            ?>
            </pre>
    </section>

    <section>
        <pre>
            <?php
highlight_string('
<?php
include "ParentClass.php";
$a = new ParentClass;
echo "<pre>";
var_dump($a);
echo "</pre>";
?>');
?>
</pre>
        <pre>
            <?php
highlight_string('
<?php
        object(ParentClass)#1 (1) {
        ["myName"]=>
        string(5) "lesha"
        }
?>');
?>
            </pre>
    </section>

    <section>
        <h3>Создание экземпляра класса</h3>
        Для создания экземпляра класса используется директива new. Новый объект всегда будет создан, за исключением случаев,
        когда он содержит конструктор, в котором определен вызов исключения в случае ошибки
    </section>

    <section>
        <pre>
            <?php
            highlight_string('
<?php
class ParentClass
{
    public $myName = "lesha";

    public function __construct()
    {
        echo "constructor of ".__CLASS__."<br />";
    }

    public function getMyName()
    {
        return $this->myName;
    }
}
?>
            ');
            ?>
        </pre>
    </section>

<section>
        <pre>
            <?php
            highlight_string('
<?php
class ChildClass
{
    public function __construct()
    {
        echo "constructor of ".__CLASS__."<br />";
    }
}
?>
            ');
            ?>
        </pre>
        <pre>
            <?php
            highlight_string('
//text of file index.php
<?php
include "ParentClass.php";
include "ChildClass.php";
    $a = new ParentClass;
    $b = new ChildClass;
?>
            ');
            ?>
        </pre>
</section>

<section>
    <h3>Работаем с методом класса</h3>
    <pre>
        <?php
        highlight_string('
<?php
include "ParentClass.php";
include "ChildClass.php";

$a = new ParentClass;
$b = new ChildClass;

$name = $a->getMyName();
echo "name is ".$name."<br />";
?>
        ');
        ?>
    </pre>
    <pre class="fragment">
        <?php
        highlight_string('
constructor of ParentClass
constructor of ChildClass
name is lesha
        ');
        ?>
    </pre>
</section>

<section>
    <h3>Доступ к свойствам и методам класса</h3>
    <ul>
        <li class="fragment">опять?</li>
        <li class="fragment">static!</li>
    </ul>
    <pre class="fragment">
        <?php
        highlight_string('
<?php
class ChildClass
{
    public static $lastName = "Smolyanov";

    public function __construct()
    {
        echo "constructor of ".__CLASS__."<br />";
    }

    public static function getFathersName()
    {
        return "Bronislavovich";
    }
}
?>
        ');
        ?>
    </pre>
</section>

<section>
    <h3>index.php</h3>
    <pre>
        <?php
        highlight_string('
<?php
include "ParentClass.php";
include "ChildClass.php";

$a = new ParentClass;
//$b = new ChildClass;

$name = $a->getMyName();
$lastName = ChildClass::$lastName;
$fathersName = ChildClass::getFathersName();
echo "<pre>";
var_dump($lastName);
echo "</pre>";
echo "<pre>";
var_dump($fathersName);
echo "</pre>";

?>
        ');
        ?>
    </pre>
</section>
<section>
    <h3>Результат</h3>
    <pre>
        <?php
        highlight_string('
constructor of ParentClass
string(9) "Smolyanov"
string(14) "Bronislavovich"
        ');
        ?>

    </pre>
</section>

<section>
    <h3>Обращение к статическому члену, self</h3>
     <pre>
        <?php
        highlight_string('
<?php
class ChildClass
{
    public static $name = "Alexei";
    public static $lastName = "Smolyanov";
    public static $fathersName = "Bronislavovich";

    public function __construct()
    {
        echo "constructor of ".__CLASS__."<br />";
    }

    public static function getFullName()
    {
        return self::$lastName." ".self::$name." ".self::$fathersName;
    }
}
?>
        ');
        ?>

    </pre>
</section>

<section>
    <h3>Результат</h3>
    <pre>
        <?php
        highlight_string('
<?php
include "ParentClass.php";
include "ChildClass.php";

//$a = new ParentClass;
//$b = new ChildClass;

$fullName = ChildClass::getFullName();
echo "person is ".$fullName;

?>');
?>
    </pre>
    <pre>
        person is Smolyanov Alexei Bronislavovich
        </pre>
</section>

<section>
    <h3>В чем разница?</h3>
    <p class="fragment">Объявление свойств и методов класса статическими позволяет обращаться к ним без создания экземпляра класса.</p>
</section>


<section>
    <h3>Наследование</h3>
    Наследование - это не просто создание точной копии класса,
    а расширение уже существующего класса, чтобы потомок мог выполнять какие-нибудь новые, характерные только ему функции.
    <hr /><p class="fragment">Ключевое слово extends</p>
</section>

<section>
    <h3>Отнаследуем класс родителя</h3>
<pre>
       <?php
    highlight_string('
<?php
class ChildClass extends ParentClass
{
    public static $name = "Alexei";
    public static $lastName = "Smolyanov";
    public static $fathersName = "Bronislavovich";

    static function getFullName()
    {
        return self::$lastName." ".self::$name." ".self::$fathersName;
    }
}
?>
    ');
        ?>
    </pre>
</section>

<section>
<pre>
    <?php
highlight_string('
<?php
include "ParentClass.php";
include "ChildClass.php";

$a = new ParentClass;
$b = new ChildClass;

$fullName = ChildClass::getFullName();
echo "person is ".$fullName;
?>
');
    ?>
</pre>
    <p>результат</p>
    <pre>
        <?php
        highlight_string('
constructor of ParentClass
constructor of ParentClass
person is Smolyanov Alexei Bronislavovich©
        ');
        ?>
    </pre>
</section>

<section>
    <h3>Наследование позволяет:</h3>
    <ul>
        <li class="fragment">DRY - don't repeat yourself</li>
        <li class="fragment">Расширять и масштабировать за счет наследования</li>
        <li class="fragment">Упорядочить структуру</li>
        <li class="fragment">Использовать сторонние сервисы, обновлять их без потерь</li>
        <li class="fragment">Меньше велосипедов ... </li>
    </ul>
</section>

<section>
    <h3>Наследование + полиморфизм</h3>
    в языках программирования — возможность объектов с одинаковой спецификацией иметь различную реализацию.
    <pre>
        <?php
highlight_string('
<?php
class ChildClass extends ParentClass
{
    public static $name = "Alexei";
    public static $lastName = "Smolyanov";
    public static $fathersName = "Bronislavovich";

    public function __construct()
    {
        echo "constructor of ".__CLASS__."<br />";
    }
}
?>
');
        ?>
        </pre>
</section>

<section>
    <p>результат:</p>
    <pre>
        <?php
        highlight_string('
constructor of ParentClass
constructor of ChildClass
person is Smolyanov Alexei Bronislavovich©
        ');
        ?>
    </pre>
</section>

<section>
    <h3>А если вот так ?</h3>
    <pre>
 <?php
highlight_string('
<?php
class ChildClass extends ParentClass
{
    public static $name = "Alexei";
    public static $lastName = "Smolyanov";
    public static $fathersName = "Bronislavovich";

    public function __construct()
    {
        parent::__construct();
        echo "constructor of ".__CLASS__."<br />";
    }

    static function getFullName()
    {
        return self::$lastName." ".self::$name." ".self::$fathersName;
    }
}
?>
');
 ?>
    </pre>
</section>

<section>
    <h3>Результат:</h3>
    <pre>
constructor of ParentClass
constructor of ParentClass
constructor of ChildClass
person is Smolyanov Alexei Bronislavovich©
        </pre>
</section>

<section>
    <h3>Подитожим по полиморфизму:</h3>
    <ul>
        <li class="fragment">Позволяет изменить поведение, переопределить метод</li>
        <li class="fragment">Позволяет вызвать родительское поведение и добавить свой функционал к нему</li>
        <li class="fragment">Добавить свой функционал, а потом завершить выполнение родительского метода</li>
    </ul>
</section>

<section>
    <h3>Модификаторы доступа</h3>
    <ul>
        <li class="fragment">public
        <p>доступно всем</p>
        </li>
        <li class="fragment">private
        <p>доступно только внутри класса</p></li>
        <li class="fragment">protected
        <p>доступно внутри класса + наследники</p></li>
    </ul>
</section>

<section>
    <h3>Функции для работы с классами и объектами</h3>
    <ul>самостоятельно
        <li class="fragment">get_class_methods()</li>
        <li class="fragment">get_class_vars()</li>
        <li class="fragment">get_object_vars()</li>
        <li class="fragment">method_exists()</li>
        <li class="fragment"> и т.д...</li>
    </ul>
</section>

<section>
    <h3>PDO</h3>
    <p>Расширение Объекты данных PHP (PDO) определяет простой и согласованный интерфейс для доступа к базам данных в PHP.
        PDO обеспечивает абстракцию (доступа к данным). Это значит, что вне зависимости от того, какая конкретная база данных используется,
        вы можете пользоваться одними и теми функциями для выполнения запросов и выборки данных.
    </p>
</section>

<section>
    <h3>Подключения и Управление подключениями</h3>
    Соединения устанавливаются автоматически при создании объекта PDO от его базового класса. Не имеет значения, какой драйвер вы хотите использовать; все что требуется, это имя базового класса.
    Конструктор класса принимает аргументы для задания источника данных (DSN), а также необязательные имя пользователя и пароль (если есть).
    <pre>
        <?php
        highlight_string('
<?php
$dbh = new PDO("mysql:host=localhost;dbname=test", $user, $pass);
?>
        ');
        ?>
    </pre>
</section>

<section>
    <h3>Формирование зпроса на выборку</h3>
    <pre>
        <?php
        highlight_string('
$login = "root";
$stmt = $db->prepare("SELECT * FROM seotm_users where login=?");
$stmt->bindParam(1,$login,PDO::PARAM_STR);
$res = $stmt->execute();
$result = $stmt->fetchAll();
echo "<pre>";
echo var_dump($result);
echo "</pre>";

foreach($result as $dataobj)
{
    echo $dataobj["login"]."<br />";
}
        ');
        ?>
    </pre>
</section>

<section>
    <pre>
        <?php
highlight_string('
object(PDO)#3 (0) {
}
array(1) {
  [0]=>
  array(20) {
    ["user_id"]=>
    string(1) "3"
    [0]=>
    string(1) "3"
    ["login"]=>
    string(4) "root"
    [1]=>
    string(4) "root"
    ["pass"]=>
    string(60) "$2a$12$LFDGPgXSceDlyduCQG0cgej6h0C7ZBfZ1HGZgW46cKuRo8VvLwb2."
    [2]=>
    string(60) "$2a$12$LFDGPgXSceDlyduCQG0cgej6h0C7ZBfZ1HGZgW46cKuRo8VvLwb2."
    ["active"]=>
    string(1) "1"
    [3]=>
    string(1) "1"
    ["email"]=>
    string(15) "info@root.zt.ua"
    [4]=>
    string(15) "info@root.zt.ua"
    ["reg_date"]=>
    string(19) "2013-03-25 12:35:02"
    [5]=>
    string(19) "2013-03-25 12:35:02"
    ["login_numbs"]=>
    string(1) "0"
    [6]=>
    string(1) "0"
    ["last_login"]=>
    string(19) "0000-00-00 00:00:00"
    [7]=>
    string(19) "0000-00-00 00:00:00"
    ["last_action_time"]=>
    string(19) "0000-00-00 00:00:00"
    [8]=>
    string(19) "0000-00-00 00:00:00"
    ["token"]=>
    string(60) "$2a$12$hVQTUeqXdGHBjlkH0iTm8OEvUEgHG7jc4KDxdrFmwROBy0I5OkZri"
    [9]=>
    string(60) "$2a$12$hVQTUeqXdGHBjlkH0iTm8OEvUEgHG7jc4KDxdrFmwROBy0I5OkZri"
  }
}
root
');
        ?>
    </pre>
</section>

</div>
</div>
