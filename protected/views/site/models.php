<?php
    $this->pageTitle = "Yii :: working with models";
?>
<div class="reveal">
<img src="/images/Yii-logo-transparent.png" />
<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">

    <section>
        <h3>Yii models (ORM)</h3>
        <span class="fragment">
           <p> we want them to be like ...</p>
        <img src="/images/models.jpg" />

            </span>

    </section>

    <section>
        <p>but they're</p>
        <span class="fragment">

        <img src="/images/orm.png" />
            </span>
    </section>

    <section>
        <h3>Бриф</h3>
        <small style="text-align: left">
            <p class="fragment"><strong>Модель (model)</strong> — это экземпляр класса CModel или класса, унаследованного от него.
            Модель используется для хранения данных и применимых к ним бизнес-правил.

            Модель представляет собой отдельный объект данных.
            Это может быть запись таблицы базы данных или HTML-форма с полями для ввода данных.
            Каждое поле объекта данных представляется атрибутом модели. Каждый атрибут имеет текстовую метку и может
            быть проверен на корректность, используя набор правил.
            </p>
            <p class="fragment">
            Yii предоставляет два типа моделей: модель формы и Active Record. Оба типа являются наследниками базового класса CModel.
            </p>

            <p class="fragment">
            <strong>Модель формы</strong> — это экземпляр класса CFormModel.
            Она используется для хранения данных, введённых пользователем.
            Как правило, мы получаем эти данные, обрабатываем, а затем избавляемся от них.
            Например, на странице авторизации модель такого типа может быть использована для представления информации
            об имени пользователя и пароле. Подробное описание работы с формами приведено в разделе Работа с формами.
            </p>

            <p class="fragment">
            <strong>Active Record (AR)</strong> — это шаблон проектирования, используемый для абстрагирования доступа к базе данных в
            объектно-ориентированной форме. Каждый объект AR является экземпляром класса CActiveRecord или класса,
            унаследованного от него, и представляет отдельную строку в таблице базы данных. Поля этой строки соответствуют
            свойствам AR-объекта. Подробнее с AR-моделью можно ознакомиться в разделе Active Record.
            </p>
        </small>

        http://www.yiiframework.com/doc/guide/1.1/ru/basics.model
    </section>

    <section>
        <h3>Модели Yii представлены классами:</h3>
        <ul>
            <li class="fragment">CFormModel (формы)</li>
            <li class="fragment">CActiveRecord (таблицы)</li>
            <li class="fragment">CModel (суперкласс)</li>
            <li class="fragment">можно свой написать =)</li>
        </ul>
    </section>

    <section>
        <h3>Модель предназначена для:</h3>
        <ul>
            <li class="fragment">Хранение данных</li>
            <li class="fragment">Бизнесс-логика, бизнесс-правила</li>
        </ul>
        <p class="fragment">наше предпочтение - "Толстые модели"</p>
    </section>

    <section>
        <h3>Базовые методы модели Active Record</h3>
<pre>
    <?php
highlight_string('
class ClassName extends CActiveRecord {

// Methods...
public function model($className=__CLASS__) {}
public function tableName() {}
public function rules() {}
public function relations() {}
public function attributeLabels() {}
public function search() {}
}
');?>
    </pre>
    </section>

    <section>
        <h3>Базовые методы модели CForm</h3>
<pre>
    <?php
    highlight_string('
class LoginForm extends CFormModel
{
// Attributes
    public $username;
    public $password;
    public $rememberMe=false;

// Methods...
public function rules() {}
public function attributeLabels() {}
}
');?>
    </pre>
        <p class="fragment">в модели форм всегда должны быть указаны атрибуты, которые мы и должны передать во вью формы</p>
    </section>

    <section>
        <h3>Экскурс по методам:</h3>
        <ul>
            <li class="fragment">
                <pre>
<?php
highlight_string('
public static function model($className = __CLASS__)
{
    return parent::model($className);
}
// возвращает модель - статический экземпляр класса AR
');
?>
                </pre>
            </li>
            <li class="fragment">
                <pre>
<?php
highlight_string('
public function tableName()
{
    return "some_table_name";
}
// возвращает имя таблицы из БД
');
?>
                </pre>
            </li>
        </ul>
    </section>

    <section>
        <h3>rules:</h3>
        <ul>
            <li class="fragment">
                <pre>
<?php
highlight_string("
    public function rules()
    {
        return array(
            array('login', 'required'),
            array('age', 'numerical', 'integerOnly' => true),
            array('text','length','max'=>'21844'), // max length for MySQL text field with UTF-8 (65535 bytes per 3 bytes for each UTF-8 char)
            array('text','filter','filter'=>array(\$obj=new CHtmlPurifier(),'purify')),
            array('captcha', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements() || !Yii::app()->user->isGuest, 'captchaAction' => 'site/captcha'),
            // SEARCH
            array('id,user_id, level, created, text', 'safe', 'on' => 'search'),
        );
    }
// устанавливает правила алидации для атрибутов модели
");
?>
                </pre>
            </li>
            </ul>
    </section>

    <section>
        <h3>Валидация</h3>
        <small style="text-align: left">
            <ul>Стандартные правила валидации

            <li>boolean: CBooleanValidator, проверят, что значение переменной равняется trueValue или falseValue.
                <li>allowEmpty, может ли значение равняться null или быть пустым.
                <li>falseValue, значение falseValue.
                <li>strict, является ли сравнение строгим: должны совпадать не только значения, но и их тип.
                <li>trueValue, значение trueValue.
                <li>captcha: CCaptchaValidator, проверяет, что значение поля модели соответствует проверочному коду CAPTCHA.
                <li>allowEmpty, может ли значение равняться null или быть пустым.
                <li>captchaAction, ID действия, показывающего изображение CAPTCHA.
                <li>caseSensitive, использовать ли регистрозависимую проверку.
                <li>compare: CCompareValidator, сравнивает значение указанного поля модели с значением другого поля и проверяет, равны ли они.
                <li>allowEmpty, может ли значение равняться null или быть пустым.
                <li>compareAttribute, имя атрибута, с которым будет производится сравнение.
                <li>compareValue, постоянное значение, с которым будет производится сравнение.
                <li>operator, оператор, используемый при сравнении.
                <li>strict, является ли сравнение строгим: должны совпадать не только значения, но и их тип.
                <li>default: CDefaultValueValidator, инициализирует атрибуты указанным значением. Валидацию при этом не выполняет. Нужен для указания значений по умолчанию.
                <li>setOnEmpty, устанавливать значение по умолчанию только если значение равно null или пустой строке.
                <li>value, значение по умолчанию.
                <li>email: CEmailValidator, проверяет, что значение является адресом email.
                <li>allowEmpty, может ли значение равняться null или быть пустым.
                <li>allowName, разрешать ли включать имя в адрес email.
                </ul>
        </small>
    </section>

<section>
    <small>
    <ul><li>checkMX, проверять ли запись MX.
        <li>checkPort, проверять ли 25-й порт.
        <li>fullPattern, регулярное выражение, используемое для проверки адреса с именем.
        <li>pattern, регулярное выражение, используемое для проверки адреса без имени.
        <li>validateIDN, проверять ли адрес с IDN (internationalized domain names, интернационализованные доменные имена). По умолчанию адрес содержащий IDN всегда будет неверным (значение false). Появилось в версии 1.1.13.
        <li>date: CDateValidator, проверяет, что значение является датой, временем или и тем и другим вместе.
        <li>allowEmpty, может ли значение равняться null или быть пустым.
        <li>format, формат значения. Может быть массивом или строкой. По умолчанию равняется 'MM/dd/yyyy'. Остальные форматы описаны в API CDateTimeParser.
        <li>timestampAttribute, имя атрибута, в который будет записан результат разбора даты. По умолчанию равен null.
        <li>exist: CExistValidator, проверяет, есть ли значение атрибута в определённой таблице.
        <li>allowEmpty, может ли значение равняться null или быть пустым.
        <li>attributeName, имя атрибута класса ActiveRecord, используемое для проверки значения.
        <li>className, имя класса ActiveRecord, используемого для проверки.
        <li>criteria, дополнительный критерий запроса.
        <li>file: CFileValidator, проверяет, был ли загружен файл.
        <li>allowEmpty, можно ли не загружать файл и оставить поле пустым.
        <li>maxFiles, максимальное количество файлов.
        <li>maxSize, максимальный размер в байтах.
        <li>minSize, минимальный размер в байтах.
        <li>tooLarge, сообщение об ошибке, выдаваемое если файл слишком большой.
        <li>tooMany, сообщение, выдаваемое если загружено слишком много файлов.
        <li>tooSmall, сообщение, выдаваемое если загруженный файл слишком мал.
        </ul>
    </small>
</section>

<section>
    <small>
        <ul>
            <li>types, список расширений файлов, которые позволено загружать.
            <li>wrongType, сообщение, выдаваемое если данный тип файла загружать нельзя.
            <li>mimeTypes, список MIME-типов файлов, которые позволено загружать. Можно использовать при условии, что установлено PECL-расширение fileinfo. Появилось в версии 1.1.11.
            <li>wrongMimeType, сообщение, выдаваемое если данный тип файла загружать нельзя. Можно использовать при условии, что установлено PECL-расширение fileinfo. Появилось в версии 1.1.11.
            <li>filter: CFilterValidator, применяет к данным фильтр.
            <li>filter, метод-фильтр.
            <li>in: CRangeValidator, проверяет, входит ли значение в заданный интервал или список значений.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>range, список допустимых значений или допустимый интервал.
            <li>strict, является ли сравнение строгим: должны совпадать не только значения, но и их тип.
            <li>not, позволяет проверить исключение из интервала вместо вхождения в него.
            <li>length: CStringValidator, проверяет, что количество введённых символов соответствует некоторому значению.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>encoding, кодировка проверяемой строки.
            <li>is, точное количество символов.
            <li>max, максимальное количество символов.
            <li>min, минимальное количество символов.
            <li>tooShort, сообщение об ошибке, выдаваемое если количество символов слишком мало.
            <li>tooLong, сообщение об ошибке, выдаваемое если количество символов слишком велико.
            <li>numerical: CNumberValidator, проверяет, что значение является числом в определённом интервале.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>integerOnly, только целые числа.

        </ul>
    </small>
</section>

<section>
    <small>
        <ul>
            <li>max, максимальное значение.
            <li>min, минимальное значение.
            <li>tooBig, сообщение об ошибке, выдаваемое если значение слишком велико.
            <li>tooSmall, сообщение об ошибке, выдаваемое если значение слишком мало.
            <li>integerPattern, регулярное выражение, используемое для валидации целых чисел. Используется тогда, когда integerOnly равно true. Появилось в версии 1.1.7.
            <li>numberPattern, регулярное выражение, используемое для валидации целых чисел. Используется тогда, когда integerOnly равно false. Появилось в версии 1.1.7.
            <li>match: CRegularExpressionValidator, проверяет, совпадает ли значение с регулярным выражением.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>pattern, регулярное выражение.
            <li>not, инвертировать ли логику валидации. Если значение равно true, то проверяемое значение не должно совпадать с регулярным выражением. Значение по умолчанию: false. Появилось в версии 1.1.5.
            <li>required: CRequiredValidator, проверяет, что значение не равно null и не является пустым.
            <li>requiredValue, значение, которое должен иметь атрибут.
            <li>strict, является ли сравнение строгим: должны совпадать не только значения, но и их тип.
            <li>safe: CSafeValidator, помечает атрибут безопасным для массового присваивания.
            <li>type: CTypeValidator, сверяет тип атрибута с указанным (integer, float, string, date, time, datetime). Для валидации дат с версии 1.1.7 лучше использовать CDateValidator.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>dateFormat, формат для валидации дат.
            <li>datetimeFormat, формат для валидации даты и времени.
            <li>timeFormat, формат для валидации времени.
            <li>type, тип данных.
            <li>unique: CUniqueValidator, проверяет значение на уникальность.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>attributeName, имя атрибута класса ActiveRecord, используемое для проверки значения.
            <li>caseSensitive, является ли сравнение регистронезависимым.
            <li>className, имя класса ActiveRecord, используемого для проверки.

        </ul>
    </small>
</section>

<section>
    <small>
        <ul>
            <li>criteria, дополнительный критерий запроса.
            <li>unsafe: CUnsafeValidator, помечает атрибут небезопасным для массового присваивания.
            <li>url: CUrlValidator, проверяет, что значения является верным URL http или https.
            <li>allowEmpty, может ли значение равняться null или быть пустым.
            <li>pattern, регулярное выражение, используемое при валидации.
            <li>validSchemes, массив с названиями допустимых схем. Схемы, допустимые по умолчанию: http и https. Появилось в версии 1.1.7.
            <li>validateIDN, проверять ли URL с IDN (internationalized domain names, интернационализованные доменные имена). По умолчанию URL содержащий IDN всегда будет неверным (значение false). Появилось в версии 1.1.13.

        </ul>
    </small>
    <p>http://yiiframework.ru/doc/cookbook/ru/form.validation.reference</p>
</section>

<section>
    <p>Имеем возможность менять текст ошибок валидации</p>
    <pre>
<?php
highlight_string("
array('email', 'email',
'message'=>'You must provide an email address
to which you have access.'),
array('pass', 'match', 'pattern'=>'/^[a-z0-9_-]{6,20}$/',
'message'=>'The password must be between 6 and 20 characters
long and can only contain letters, numbers, the underscore,
and the hyphen.'),
");
?>
</pre>
    <span>
        <p>а так же устанавливать значения по умолчанию</p>
        <pre>
<?php
highlight_string("
array('description', 'default', 'value'=>NULL)
");
?>
        </pre>
    </span>
</section>

<section>
    <h3>Собственные валидаторы</h3>
<pre>
<?php
highlight_string("
public function authenticate(\$attribute,\$params) {
    if(!\$this->hasErrors()) {
        \$this->_identity=new UserIdentity(\$this->username,
        \$this->password);
    if(!\$this->_identity->authenticate())
        \$this->addError('password',
        'Incorrect username or password.');
    }
}

rules :

array('password', 'authenticate'),

");
?>
</pre>
</section>

<section>
    <h3>Сценарии валидации</h3>
    <small><p>
            Мы можем задать сценарий через свойство scenario, таким образом указав, какой набор правил будет использован для проверки.

            Проверка выполняется в зависимости от сценария. Свойство scenario задаёт сценарий, в котором используется
            модель, и определяет, какой набор правил валидации будет использован. К примеру, в сценарии login мы
            хотим проверить только поля модели пользователя username и password.
            В сценарии register нам необходимо проверять большее количество данных: email, address и т.д.
        </p></small>
<pre>
<?php
highlight_string("
\$model=new User('register');
\$model->scenario = 'value';.

array('id, username, email, pass, type, date_entered', 'safe',
'on'=>'search'),

array('date_entered', 'default','value'=>new CDbExpression('NOW()'),
'on'=>'insert'),
array('date_updated', 'default','value'=>new CDbExpression('NOW()'),
'on'=>'update'),


");
?>
</pre>
</section>

<section>
    <h3>немного подитожим:</h3>
    <ul>
        <li class="fragment">Определяем обязательные атрибуты</li>
        <li class="fragment">Валидируем по-возможности все что можно валидировать (полный контроль над входящими данными)</li>
        <li class="fragment">Применяем фильтры где есть возможность и необходимость</li>
        <li class="fragment">Только необходимые атрибуты в safe</li>
        <li class="fragment">Только необходимые атрибуты в search</li>
        <li class="fragment">Собственные лаконичные сообщения об ошибках (если нужно)</li>
    </ul>
</section>

<section>
    <h3>field labels</h3>
<pre>
<?php
highlight_string("
public function attributeLabels() {
return array(
'id' => 'АйДи',
'user_id' => 'Uploaded By',
'name' => 'Имя Файла',
'type' => 'File Type',
'size' => 'File Size',
'description' => 'Описание',
'date_entered' => 'Date Entered',
'date_updated' => 'Date Updated',
);
}

");
?>
</pre>
</section>

<section>
    <h3>События моделей</h3>
    <ul>
        <li>afterConstruct()
        <li>afterDelete()
        <li>afterFind()
        <li>afterSave()
        <li>afterValidate()
        <li>beforeDelete()
        <li>beforeFind()
        <li>beforeSave()
        <li>beforeValidate()
    </ul>
    </section>

    <section>
        <h3>CComponent</h3>
        <img src="/images/ccomponent.png"/>
        <ul> CComponent позволяет получить возможность:
            <li>использовать get/set для доступа к атрибутам</li>
            <li>использовать события</li>
            <li>использовать поведения (расширение функционала)</li>
        </ul>
    </section>

    <section>
        <h3>примерчик</h3>
        <pre>
<?php
highlight_string("
public function onCreate(\$event)
{
    // Непосредственно вызывать событие принято в его описании.
    // Это позволяет использовать данный метод вместо raiseEvent
    // во всём остальном коде.
    \$this->raiseEvent('onCreate', \$event);
}

public function onRenewPassword(\$event)
{
    \$this->raiseEvent('onRenewPassword', \$event);
}
");?>
</pre>
        <pre>
<?php highlight_string("
public function actionRenewPassword()
    {
        \$id = Yii::app()->request->getParam('user',null);
        if(\$id){
            \$model = Users::model()->findByPk(\$id);
            \$model->active = 0;
            \$obj = new bCrypt();
            \$model->token = \$obj->hash(time());
            \$model->save(false);
            \$event = new CEvent(\$this,array('model'=>\$model));
            \$notifier = new Notifier();
            \$model->onRenewPassword = array(\$notifier, 'newUserPassword');
            \$model->onRenewPassword(\$event);
        }
    }
");
?>
    </pre>
</section>



<section>
    <h3>Relations бриф</h3>
    <small>
        <p>
        Связь между двумя AR-классами напрямую зависит от связей между соответствующими таблицами базы данных.
        С точки зрения БД, связь между таблицами A и В может быть трёх типов: один-ко-многим (например, tbl_user и tbl_post),
        один-к-одному (например, tbl_user и tbl_profile) и многие-ко-многим (например, tbl_category и tbl_post).
        В AR существует четыре типа связей:
        </p>
        <p>
        <strong>BELONGS_TO</strong>: если связь между А и В один-ко-многим, значит В принадлежит А (например, Post принадлежит User);
        </p>
        <p>
        <strong>HAS_MANY</strong>: если связь между таблицами А и В один-ко-многим, значит у А есть много В (например, у User есть много Post);
        </p>
        <p>
        <strong>HAS_ONE</strong>: это частный случай HAS_MANY, где А может иметь максимум одно В (например, у User есть только один Profile);
        </p>
        <p>
        <strong>MANY_MANY</strong>: эта связь соответствует типу связи многие-ко-многим в БД. Поскольку многие СУБД не поддерживают
        непосредственно этот тип связи, требуется ассоциативная таблица для преобразования связи многие-ко-многим
        в связи один-ко-многим. В нашей схеме базы данных этой цели служит таблица tbl_post_category.
        В терминологии AR связь MANY_MANY можно описать как комбинацию BELONGS_TO и HAS_MANY. Например,
        Post принадлежит многим Category, а у Category есть много Post.
        </p>
    </small>

</section>

<section>
    <h3>relations method</h3>
<pre>
<?php
highlight_string("
public function relations() {
    return array(
        'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        'user' => array(self::BELONGS_TO, 'User', 'user_id'),
    );
}

public function relations() {
    return array(
        'comments' => array(self::HAS_MANY, 'Comment', 'user_id'),
        'files' => array(self::HAS_MANY, 'File', 'user_id'),
        'pages' => array(self::HAS_MANY, 'Page', 'user_id'),
    );
}

");
?>
</pre>
</section>

<section>search method
    <p>Необходим для фильтрации данных</p>
</section>
</div>
</div>
