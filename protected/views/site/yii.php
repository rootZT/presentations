<div class="reveal">
<img src="/images/Yii-logo-transparent.png" />
<!-- Any section element inside of this container is displayed as a slide -->
    <div class="slides">

        <section>
            <h3>Yii</h3>
            <h3 class="fragment">Yes it is!</h3>
            <small class="fragment">Фреймворк — структура программной системы; программное обеспечение, облегчающее разработку и объединение
                разных компонентов большого программного проекта. Употребляется также слово «каркас», а некоторые авторы
                используют его в качестве основного, в том числе не базируясь вообще на англоязычном аналоге.
                Можно также говорить о каркасном подходе как о подходе к построению программ, где любая конфигурация
                программы строится из двух частей: первая, постоянная часть — каркас, не меняющийся от конфигурации к
                конфигурации и несущий в себе гнезда, в которых размещается вторая, переменная часть — сменные модули
                .</small>
        </section>

        <section>
            <h3>Зачем использовать фреймворк?</h3>
            <ul>
                <li class="fragment">Более быстрая разработка</li>
                <li class="fragment">Более высокий уровень безопасности</li>
                <li class="fragment">Отлаженный разработчиками код</li>
                <li class="fragment">Улучшенная архитектура приложения</li>
            </ul>
        </section>

        <section>
            <h3>Когда не стоит использовать фреймворк?</h3>
            <ul>
                <li class="fragment">Нет времени изучать</li>
                <li class="fragment">Не все задачи решаются бытрее</li>
                <li class="fragment">Излишние затраты ресурсов</li>
                <li class="fragment">Обновление версий (безопасность, улучшения)</li>
            </ul>
        </section>
        <section>
            <h3>Почему Yii?</h3>
            <p class="fragment">Is it fast?...Is it secure?...Is it professional?...Is it right for my next
                project?...
            </p>
            <h3 class="fragment">Yes, it is!</h3>
        </section>

        <section>
            <h3>Что yii предлагает нам:</h3>
            <ul>
                <li class="fragment">Использование ООП подхода</li>
                <li class="fragment">PHP 5+, PHP 5.3+ (Yii2)</li>
                <li class="fragment">MVC</li>
                <li class="fragment">ActiveRecord (ORM), DAO, PDO</li>
                <li class="fragment">Шаблоны</li>
                <li class="fragment">Кеширование</li>
                <li class="fragment">кодогенератор CRUD</li>
                <li class="fragment">I18n</li>
                <li class="fragment">и многое другое ...</li>

            </ul>
        </section>

        <section>
            <h3>Что нужно иметь для комфортной работы с Yii?</h3>
            <ul>
                <li class="fragment">Значительный опыт в веб-разработке</li>
                <li class="fragment">Уверенные знания HTML, PHP, MySQL, SQL </li>
                <li class="fragment">Не бояться JavaScript & jQuery</li>
                <li class="fragment">Понимание того, что трудности, тяготы и лишения - составляющие любого учебного процесса =)</li>
        </section>

        <section>
            <h3>MVC</h3>
            <img class="fragment" src="/images/MVC.png" />
        </section>

        <section>
            <h3>Обработка запроса</h3>
            <img src="/images/yii-request-flow.png" />
        </section>

        <section>
            <h3>Узкие места:</h3>
            <ul>
                <li class="fragment">Размещение логики (модель? контроллер ? WTF?)</li>
                <li class="fragment">Работа с данными в контроллере</li>
                <li class="fragment">Генерация HTML в модели</li>
            </ul>
        </section>

        <section>
            <h3>Разворачивание приложения:</h3>
            <ul>
                <li class="fragment">Скачать framework http://yiiframework.com</li>
                <li class="fragment">Проверить минимальные требования (requirements folder)</li>
                <li class="fragment">Установить (распаковать) фреймворк</li>
                <li class="fragment">Развернуть каркас приложения (yiic webapp /var/www/testproject)</li>
                <li class="fragment">Проверить или заработало</li>
            </ul>
        </section>

        <section>
            <h3>Состав приложения:</h3>
            <ul>
                <li class="fragment">assets</li>
                <li class="fragment">css</li>
                <li class="fragment">protected</li>
                <li class="fragment">themes</li>
                <li class="fragment">index-test.php</li>
                <li class="fragment">index.php</li>
            </ul>
        </section>

        <section>
            <h3>Protected:</h3>
            <ul>
                <li class="fragment">commands (yiic commands)</li>
                <li class="fragment">components</li>
                <li class="fragment">config</li>
                <li class="fragment">controllers</li>
                <li class="fragment">data</li>
                <li class="fragment">extensions</li>
                <li class="fragment">messages</li>
                <li class="fragment">migrations</li>
                <li class="fragment">models</li>
                <li class="fragment">runtime</li>
                <li class="fragment">tests</li>
                <li class="fragment">views</li>
            </ul>
        </section>

        <section>
            <h3>Соглашения по именованию:</h3>
            <ul>
                <li>Контроллеры</li>
                <li>Модели</li>
                <li>Отображения (+ _partials)</li>
            </ul>
        </section>

        <section>
            <h3>index.php</h3>
            <img src="/images/1.png" />
        </section>

        <section>
            <h3>Файл конфигурации main.php</h3>
            <ul>
                <li class="fragment">preload</li>
                <li class="fragment">import</li>
                <li class="fragment">modules</li>
                <li class="fragment">components</li>
                <li class="fragment">params</li>
            </ul>
        </section>

        <section>
            <small>
            <ul>Встроенные компоненты
                <li>assetManager: CAssetManager — управляет публикацией файлов ресурсов (asset files);
                <li>authManager: CAuthManager — контролирует доступ на основе ролей (RBAC);
                <li>cache: CCache — предоставляет возможности кэширования данных; учтите, что вы должны указать используемый класс (например, CMemCache, CDbCache), иначе при обращении к компоненту будет возвращен null;
                <li>clientScript: CClientScript — управляет клиентскими скриптами (JavaScript и CSS);
                <li>coreMessages: CPhpMessageSource — предоставляет переводы системных сообщений Yii-фреймворка;
                <li>db: CDbConnection — обслуживает соединение с базой данных; обратите внимание, что для использования компонента необходимо установить свойство connectionString;
                <li>errorHandler: CErrorHandler — обрабатывает не пойманные ошибки и исключения PHP;
                <li>format: CFormatter — форматирует данные для их последующего отображения.
                <li>messages: CPhpMessageSource — предоставляет переводы сообщений, используемых в Yii-приложении;
                <li>request: CHttpRequest — содержит информацию о пользовательском запросе;
                <li>securityManager: CSecurityManager — предоставляет функции, связанные с безопасностью (например, хеширование, шифрование);
                <li>session: CHttpSession — обеспечивает функциональность, связанную с сессиями;
                <li>statePersister: CStatePersister — предоставляет метод для сохранения глобального состояния;
                <li>urlManager: CUrlManager — предоставляет функции парсинга и формирования URL;
                <li>user: CWebUser — предоставляет идентификационную информацию текущего пользователя;
                <li>themeManager: CThemeManager — управляет темами оформления.
            </ul>
            </small>
        </section>

        <section>
            <ul> Жижненный цыкл приложения
                <li>Предварительная инициализация приложения через CApplication::preinit().
                <li>    Инициализация автозагрузчика классов и обработчика ошибок.
                <li>Регистрация компонентов ядра.
                <li>Загрузка конфигурации приложения.
                <li>Инициализация приложения CApplication::init():
                <li>регистрация поведений приложения;
                <li>загрузка статических компонентов приложения.
                <li>Вызов события onBeginRequest.
                <li>Обработка запроса:
                <li>сбор информации о запросе;
                <li>создание контроллера;
                <li>запуск контроллера.
                <li>Вызов события onEndRequest.
            </ul>
        </section>

        <section>
            <h3>Контроллер</h3>
            <small>
                Контроллер (controller) — это экземпляр класса CController или унаследованного от него класса.
                Он создается объектом приложения в случае, когда пользователь его запрашивает.
                При запуске контроллер выполняет соответствующее действие, что обычно подразумевает создание
                соответствующих моделей и отображение необходимых представлений. В самом простом случае действие — это метод класса контроллера, название которого начинается на action.

                У контроллера есть действие по умолчанию, которое выполняется в случае, когда пользователь
                не указывает действие при запросе. По умолчанию это действие называется index.
                Изменить его можно путём установки значения CController::defaultAction.
            </small>
        </section>

        <section>
            <h3>Обработка запросов контроллером</h3>
            <ul>
            <li>http://www.example.com/
                <li>http://www.example.com/index.php
                <li>http://www.example.com/index.php?r=site
                <li>http://www.example.com/index.php?r=site/login
                <li>http://www.example.com/site/login/
                <li>http://www.example.com/page/35/
            </ul>
        </section>

        <section>
            <h3>Изменение базовой конфигурации, ЧПУ</h3>
            <ul>
                <li class="fragment">url, GET/POST</li>
                <li class="fragment">ЧПУ - это ...</li>
                <li class="fragment">компонент urlManager</li>
                <li class="fragment">index.php .htaccess</li>
            </ul>
        </section>

        <section>
            <h3>Модель</h3>
            <small>
                <p>    Модель (model) — это экземпляр класса CModel или класса, унаследованного от него.
                Модель используется для хранения данных и применимых к ним бизнес-правил.
                </p>
                <p>
                Модель представляет собой отдельный объект данных.
                Это может быть запись таблицы базы данных или HTML-форма с полями для ввода данных.
                Каждое поле объекта данных представляется атрибутом модели. Каждый атрибут имеет текстовую метку и может
                быть проверен на корректность, используя набор правил.
                </p>
                <p>
                Yii предоставляет два типа моделей: модель формы и Active Record.
                Оба типа являются наследниками базового класса CModel.
                </p>
                <p>
                Active Record (AR) — это шаблон проектирования, используемый для абстрагирования доступа к базе данных
                в объектно-ориентированной форме. Каждый объект AR является экземпляром класса CActiveRecord или класса,
                унаследованного от него, и представляет отдельную строку в таблице базы данных. Поля этой строки соответствуют свойствам AR-объекта.
                </p>
            </small>
        </section>

        <section>
            <h3>Представление</h3>
            <small>
                <p>
                Представление — это PHP-скрипт, состоящий преимущественно из элементов пользовательского интерфейса.
                Он может включать выражения PHP, однако рекомендуется, чтобы эти выражения не изменяли данные и оставались относительно простыми.
                Следуя концепции разделения логики и представления, большая часть кода логики должна быть помещена в контроллер или модель, а не в скрипт представления.
                </p>

                <p>
                    Виджет (widget) — это экземпляр класса CWidget или унаследованного от него.
                    Это компонент, применяемый, в основном, с целью оформления. Виджеты обычно встраиваются в представления
                    для формирования некоторой сложной, но в то же время самостоятельной части пользовательского интерфейса.
                    К примеру, виджет календаря может быть использован для рендеринга сложного интерфейса календаря.
                    Виджеты позволяют повторно использовать код пользовательского интерфейса.
                </p>
            </small>
        </section>

        <section>
            <h3>Модуль</h3>
            <small>
                <p>Модуль — это самодостаточная программная единица, состоящая из моделей, представлений, контроллеров и иных компонентов.
                    Во многом модуль схож с приложением. Основное различие заключается в том, что модуль не может
                    использоваться сам по себе — только в составе приложения. Пользователи могут обращаться к контроллерам
                    внутри модуля абсолютно так же, как и в случае работы с обычными контроллерами приложения.
                </p>
                <p>
                Модули могут быть полезными в нескольких ситуациях. Если приложение очень объёмное, мы можем разделить
                    его на несколько модулей, разрабатываемых и поддерживаемых по отдельности. Кроме того, некоторый часто
                    используемый функционал, например, управление пользователями, комментариями и пр., может разрабатываться как модули,
                    чтобы впоследствии можно было с лёгкостью воспользоваться им вновь.
                </p>

            </small>
        </section>

        <section>
            <h3>Gii-module</h3>
            <ul>CRUD
                <li class="fragment">Create</li>
                <li class="fragment">Read</li>
                <li class="fragment">Update</li>
                <li class="fragment">Delete</li>
            </ul>
        </section>

    </div>
</div>
