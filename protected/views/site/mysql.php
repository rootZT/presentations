<div class="reveal">

<!-- Any section element inside of this container is displayed as a slide -->
<div class="slides">
    <section>
        <h3>Базы данных</h3>
        <p class="fragment">База данных — совокупность данных,
            хранимых в соответствии со схемой данных, манипулирование которыми выполняют
            в соответствии с правилами средств моделирования данных.</p>
    </section>
    <section>
        <h3>MYSQL</h3>
        <p class="fragment">Mysql - реляционная база данных</p>
        <p class="fragment">Реляционная база данных это тело связанной информации, сохраняемой в двухмерных таблицах.
            Это напоминает адресную или телефонную книгу.</p>
    </section>

    <section>
      <h3>Типичная база данных</h3>
        <pre>
       ----------------------------------------------
        CNUM  |  CNAME     | CITY    | RATING | SNUM
       -------|------------|---------|--------|------
        2001  |  Hoffman   | London  |   100  | 1001
        2002  |  Giovanni  | Rome    |   200  | 1003
        2003  |  Liu       | SanJose |   200  | 1002
        2004  |  Grass     | Berlin  |   300  | 1002
        2006  |  Clemens   | London  |   100  | 1001
        2008  |  Cisneros  | SanJose |   300  | 1007
        2007  |  Pereira   | Rome    |   100  | 1004
       ----------------------------------------------
            </pre>
        </section>
    <section>
        <h3>разбор</h3>
        <pre>
       ----------------------------------------------
        CNUM  |  CNAME     | CITY    | RATING | SNUM
       -------|------------|---------|--------|------
        2001  |  Hoffman   | London  |   100  | 1001
       ----------------------------------------------
            </pre>
        <ul>
            <li class="fragment">База данных</li>
            <li class="fragment">Таблица</li>
            <li class="fragment">Столбцы (колонки)</li>
            <li class="fragment">Строки</li>
        </ul>
    </section>

    <section>
        <h3>Создание базы данных mysql</h3>
        <ul>
<li class="fragment">Подключение к серверу БД
<pre>
lesha@rootzt:~$ mysql -u root -p
</pre>
</li>
            <li class="fragment">строка приветствия mysql
<pre>
mysql>
</pre>
            </li>
            <li class="fragment">создание БД mysql
<pre>
mysql> create database mynewdb;
Query OK, 1 row affected (0.24 sec)
            </pre>
            </li>
            <li class="fragment">использование новосозданной БД
<pre>
mysql> use mynewdb;
Database changed
            </pre>
            </li>
        </ul>
    </section>

    <section>
        <h3>Таблицы БД</h3>
        <ul>
            <li class="fragment">Посмотрим какие уже есть таблицы
<pre>
mysql> show tables;
Empty set (0.00 sec)
    </pre>
            </li>
            <li class="fragment">Создадим новую, пустую таблицу
<pre>
mysql> create table test;
ERROR 1113 (42000): A table must have at least 1 column
    </pre>
            </li>
            </ul>
    </section>

    <section>
        <h3>Колонки (стобцы)</h3>
        <ul>Для того, чтобы создать столбец нужно:
            <li class="fragment">
                Иметь данные необходимые для сохранения :-)
            </li>
            <li class="fragment">
                Определить типы данных, которые нужно хранить
            </li>
            <li class="fragment">
                Ознакомиться с нормализацией данных
            </li>
            <li class="fragment">
                Поехали ..!
            </li>
        </ul>
    </section>

    <section>
        <h3>Структура БД</h3>
        <ul>
            <li class="fragment">Определить к какой нормальной форме привести структуру</li>
            <li class="fragment">Согласно форме построить связи между таблицами (если неободимо)</li>
            <li class="fragment">Назначить колонкам типы</li>
        </ul>
    </section>

    <section>
        <h3>Типизация в БД</h3>
        <ul>
            <li class="fragment">Зачем вообще нужна?</li>
            <li class="fragment">Основные типы данных:
                    <ul>
                        <li class="fragment">Числовые</li>
                        <li class="fragment">Дата и время</li>
                        <li class="fragment">Строковые</li>
                        <li class="fragment">Spatial - спецтип (геоетрический)</li>
                    </ul>
            </li>
        </ul>
    </section>

    <section>
        <h3>Числовые типы</h3>
        <ul>Целые числа
            <li class="fragment">TINYINT	Может хранить числа от -128 до 127</li>
            <li class="fragment">SMALLINT	Диапазон от -32 768 до 32 767</li>
            <li class="fragment">MEDIUMINT	Диапазон от -8 388 608 до 8 388 607</li>
            <li class="fragment">INT	Диапазон от -2 147 483 648 до 2 147 483 647</li>
            <li class="fragment">BIGINT	Диапазон от -9 223 372 036 854 775 808 до 9 223 372 036 854 775 807</li>
        </ul>
    </section>

    <section>
        <h3>Числовые типы</h3>
        <ul>Дробные числа
            <li class="fragment">UNSIGNED
                - задает беззнаковые числа.</li>
            <li class="fragment">FLOAT
                Число с плавающей точкой небольшой точности.</li>
            <li class="fragment">DOUBLE
                Число с плавающей точкой двойной точности.</li>
            <li class="fragment">REAL
                Синоним для DOUBLE.</li>
            <li class="fragment">DECIMAL
                Дробное число, хранящееся в виде строки.</li>
            <li class="fragment">NUMERIC
                Синоним для DECIMAL.</li>
        </ul>
    </section>

    <section>
        <h3>Строки</h3>
        <ul>
            <li class="fragment">VARCHAR	Может хранить не более 255 символов.</li>
            <li class="fragment">TINYTEXT	Может хранить не более 255 символов.</li>
            <li class="fragment">TEXT	Может хранить не более 65 535 символов.</li>
            <li class="fragment">MEDIUMTEXT	Может хранить не более 16 777 215 символов.</li>
            <li class="fragment">LONGTEXT	Может хранить не более 4 294 967 295 символов.</li>
        </ul>
    </section>

    <section>
        <h3>Бинарные данные</h3>
        <ul>
            <li class="fragment">TINYBLOB	Может хранить не более 255 символов.</li>
            <li class="fragment">BLOB	Может хранить не более 65 535 символов.</li>
            <li class="fragment">MEDIUMBLOB	Может хранить не более 16 777 215 символов.</li>
            <li class="fragment">LONGBLOB	Может хранить не более 4 294 967 295 символов.</li>
        </ul>
    </section>

    <section>
        <h3>Дата и время</h3>
        <ul>
            <li class="fragment">DATE	Дата в формате ГГГГ-ММ-ДД</li>
            <li class="fragment">TIME	Время в формате ЧЧ:ММ:СС</li>
            <li class="fragment">DATETIME	Дата и время в формате ГГГГ-ММ-ДД ЧЧ:ММ:СС</li>
            <li class="fragment">TIMESTAMP	Дата и время в формате timestamp.</li>
        </ul>
    </section>

    <section>
        <h3>Создание таблицы</h3>
        <pre>
mysql> create table users (
    -> id int,
    -> login varchar(128),
    -> email varchar(64),
    -> role varchar(32)
    -> );
Query OK, 0 rows affected (0.13 sec)
        </pre>
<pre>
mysql> show tables;
+-------------------+
| Tables_in_mynewdb |
+-------------------+
| users             |
+-------------------+
1 row in set (0.00 sec)
    </pre>
    </section>

<section>
    <h3>Что внутри?</h3>
    <pre>
        mysql> describe users;
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| id    | int(11)      | YES  |     | NULL    |       |
| login | varchar(128) | YES  |     | NULL    |       |
| email | varchar(64)  | YES  |     | NULL    |       |
| role  | varchar(32)  | YES  |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+
4 rows in set (0.10 sec)
    </pre>
</section>

<section>
    <h3>Первичный ключ? - не, не слышал =)</h3>
    <span class="fragment">
        Первичный ключ (primary key) представляет собой один из примеров уникальных индексов и применяется
        для уникальной идентификации записей таблицы. Никакие из двух записей таблицы не могут иметь одинаковых значений первичного ключа.
        Первичный ключ обычно сокращенно обозначают как PK (primary key).
    </span>
</section>

<section>
    <h3>Добавим первичный ключ</h3>
    <pre>
mysql> alter table users
    -> modify id
    -> int unsigned not null auto_increment,
    -> add primary key (id);
Query OK, 0 rows affected (0.53 sec)
Records: 0  Duplicates: 0  Warnings: 0

        ----------------------

mysql> describe users;
+-------+------------------+------+-----+---------+----------------+
| Field | Type             | Null | Key | Default | Extra          |
+-------+------------------+------+-----+---------+----------------+
| id    | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| login | varchar(128)     | YES  |     | NULL    |                |
| email | varchar(64)      | YES  |     | NULL    |                |
| role  | varchar(32)      | YES  |     | NULL    |                |
+-------+------------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)
        </pre>
</section>

<section>
    <h3>Что дальше?</h3>
    <ul>
        <li class="fragment">Создаем остальные таблицы, для полноты картины</li>
        <li class="fragment">Учимся выбирать,добавлять, удалять, изменять данные в таблицах</li>
    </ul>
</section>

<section>
    <h3>Вставка данных</h3>
    <pre>
mysql> insert into users
    -> (id,login,email,role)
    -> values
    -> ('','lesha','root@root.zt.ua','admin');
Query OK, 1 row affected, 1 warning (0.11 sec)

mysql> select * from users;
+----+-------+-----------------+-------+
| id | login | email           | role  |
+----+-------+-----------------+-------+
|  1 | lesha | root@root.zt.ua | admin |
+----+-------+-----------------+-------+
1 row in set (0.00 sec)
    </pre>
</section>

<section>
    <h3>Изменение данных</h3>
    <pre>
mysql> update users
    -> set email='info@root.zt.ua'
    -> where id='1';
Query OK, 1 row affected (0.17 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from users;
+----+-------+-----------------+-------+
| id | login | email           | role  |
+----+-------+-----------------+-------+
|  1 | lesha | info@root.zt.ua | admin |
+----+-------+-----------------+-------+
1 row in set (0.00 sec)
    </pre>
</section>

<section>
    <h3>Удаление данных</h3>
    <pre>
mysql> delete from users where id='1';
Query OK, 1 row affected (0.06 sec)

mysql> select * from users;
Empty set (0.00 sec)
    </pre>
</section>

<section>
    <h3>Выборка + условия</h3>
    <ul>
        <li class="fragment">SELECT *</li>
        <li class="fragment">SELECT + WHERE</li>
        <li class="fragment">SELECT + ORDER</li>
        <li class="fragment">SELECT + GROUP</li>
        <li class="fragment">SELECT + DISTINCT</li>
        <li class="fragment">SELECT + COUNT</li>
    </ul>
</section>

<section>
    <h3>Mysql JOIN</h3>
    <img src="/images/SQL_JOINS.jpg" />
</section>

<section>
    <h3>Mysql + PHP</h3>
    <pre>
<code>
<?php
highlight_string('
<?php
    $db_hostname = "localhost";
    $db_database = "mynewdb;"
    $db_username = "root";
    $db_password = "";
?>
');
?>
</code>
    </pre>
</section>


    <section>
        <h3>Подключение к mysql через PHP</h3>
    <pre>
<code>
    <?php
    highlight_string('
<?php
    $db_server = mysql_connect($db_hostname,$db_username,$db_password);
    if(!$db_server)
        die("Unable to establish connection to DB");
?>
');
    ?>
</code>
    </pre>
    </section>

    <section>
        <h3>Функции обработки результатов запроса</h3>

        Если запрос, выполненный с помощью функции mysql_query() успешно выполнился,
        то в результате клиент получит набор записей, который может быть обработан следующими функциями PHP:
        <ul>
        <li>mysql_result() - получить необходимый элемент из набора записей;
            <li>mysql_fetch_array() - занести запись в массив;
            <li>mysql_fetch_row() - занести запись в массив;
            <li>mysql_fetch_assoc() - занести запись в ассоциативный массив;
            <li>mysql_fetch_object() - занести запись в объект.
            </ul>
    </section>

<section>
    <h3>Пример</h3>
    <pre>
<code>
    <?php
    highlight_string('
<?php
$host = "localhost";
$user = "user";
$password = "secret_password";

// Производим попытку подключения к серверу MySQL:
if (!mysql_connect($host, $user, $password))
{
echo "<h2>MySQL Error!</h2>";
exit;
}

// Выбираем базу данных:
mysql_select_db($db);

// Выводим заголовок таблицы:
echo "<table border=\"1\" width=\"100%\" bgcolor=\"#FFFFE1\">";
echo "<tr><td>Email</td><td>Имя</td><td>Месяц</td>";
echo "<td>Число</td><td>Пол</td></tr>";

// SQL-запрос:
$q = mysql_query ("SELECT * FROM mytable");

// Выводим таблицу:
for ($c=0; $c<mysql_num_rows($q); $c++)
{
echo "<tr>";

$f = mysql_fetch_array($q);
echo "<td>$f[email]</td><td>$f[name]</td><td>$f[month]</td>";
echo "<td>$f[day]</td><td>$[s]</td>";

echo "</tr>";
}
echo "</table>";
?>
');
    ?>
</code>
    </pre>
</section>

</div>